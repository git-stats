#!/usr/bin/env python

import os

from git import Repo

class OptionList:
  """OptionList class
  """

  pass

def _readVerse(verse, config, convert_camel_case):
  """Reads in a verse from a config file and stores the result in config

  verse: The lines for the current verse, not including the header.
  config: A dictionary to store the found configurations in.
  convert_camel_case: Whether to convert camelCase words to dashed_form.
  """

  for line in verse:
    # Remove spaces and newlines
    stripline = line.lstrip().rstrip()

    # Stop if the line is not left indented, or empty
    if stripline == line.rstrip() or not stripline:
      break

    # Extract the key:value pair
    splitline = stripline.split(' = ')
    key = splitline[0].rstrip()
    value = splitline[1].lstrip()

    # Try to convert to a number
    try:
      intval = int(value)
      value = intval
    except ValueError:
      pass

    # Try to convert boolean values
    if value == "False":
      value = False

    if value == "True":
      value = True

    if value == "None":
      value = None

    # If desired, convert camelCase to dashed_form
    if convert_camel_case and key.lower() != key:
      dashed_form = ""

      for c in key:
        if c.isupper():
          dashed_form += '_'

        dashed_form += c.lower()

      key = dashed_form

    # Store the parsed and converted result
    config[key] = value

def read(lines, convert_camel_case=True):
  """Reads in the specified file

  Understands 'True', 'False', 'None' and integer values.
  All camelCase keys are converted to dashed_form.
  The file is expected to have the following format:
  .*
  [GitStats]
    [key = value]*
  <any unindented or empty line>
  .*

  Args:
    path: The location of the file to read in

  Returns: A dictionary with the key:value pairs specified in the file.
  """

  config = {}

  while True:
    # Find our verse
    try:
      pos = lines.index("[GitStats]\n")
    except ValueError:
      try:
        pos = lines.index("[GitStats]")
      except ValueError:
        return config

    # Kick off the header
    pos += 1
    lines = lines[pos:]

    # Read in this verse, updates config with the found configurations
    _readVerse(lines, config, convert_camel_case)

  return config

def _getDefaultConfigPaths():
  """Returns a list of config paths to use by default
  """

  paths = []

  path = "~/.gitconfig"
  path = os.path.expanduser(path)
  paths.append(path)

  repo = Repo(".")
  path = os.path.join(repo.wd, '.git', 'config')
  paths.append(path)

  paths.append("config")

  return paths

def _readConfigPaths(paths, convert_camel_case):
  """Reads the config files at the specified paths

  When multiple values for a key are read in, the one that
  is read in the last is used. (E.g., when reading in the
  config files, no effort is made to prevent existing
  values being overwritten).

  Args:
    paths: The paths to the config files.
    convert_camel_case: Whether to convert camelCase to dashed_form.
  """

  result = {}

  # Check all the configuration files and parse them
  for path in paths:
    if not os.path.isfile(path):
      continue

    file = open(path)
    lines = file.readlines()

    config = read(lines, convert_camel_case)

    # Add the found keys to our result
    for key, value in config.iteritems():
      result[key] = value

  return result

def readDefaultConfigs(convert_camel_case=True):
  """Reads all default config files and returns the result

  When a value is read in multiple times the one read in
  the last is used. The following files are tried in the
  listed order:
  ~/.gitconfig
  .git/config
  config
  """

  paths = _getDefaultConfigPaths()
  result = _readConfigPaths(paths, convert_camel_case)

  return result

def extractOptions(options, options_list):
  """Extracts options and returns the result

  The options are extracted from the specified options and
  from the 'config' file.
  """

  opts = readDefaultConfigs()

  result = OptionList()

  for opt in options_list:
    if getattr(options, opt, None) == None:
      val = opts.get(opt, None)
    else:
      val = getattr(options, opt)

    if val == None and getattr(result, opt, None) != None:
      continue

    setattr(result, opt,  val)

  return result

def main(args):
  """Parses all config files and prints the result

  Runs once with convert_camel_case on, and once with it off.

  Params:
    args: Not used
  """

  result = readDefaultConfigs()

  print("convert_camel_case is ON:")
  for key, value in result.iteritems():
    print("%s: %s" % (key, str(value)))

  result = readDefaultConfigs(False)

  print("")
  print("convertCamelCase is OFF:")
  for key, value in result.iteritems():
    print("%s: %s" % (key, str(value)))

if __name__ == '__main__':
  import sys
  main(sys.argv)

