#!/usr/bin/env python

import os
import sys
import re

from optparse import OptionParser, OptionValueError
from git import Repo

from git_stats import parse

GIT_STATS_PRETTY_PRINT = os.environ.get("GIT_STATS_PRETTY_PRINT", "full")

def _logContains(log, regexp):
  """Traverses the specified log and searches for the specified regexp.

  If the first line of the log starts with 'diff' only
  lines that start with '+' or '-' will be examined.

  Params:
    log: The log to search through.
    regexp: The regexp to match for.
  """

  matcher = re.compile(regexp)

  isDiff = log[0].startswith("diff")

  for line in log:
    # Skip anything that is not an addition or a removal
    if isDiff and not line.startswith("+") and not line.startswith("-"):
        continue      

    # If this line matches the regexp, accept
    if matcher.search(line):
      return True

  # None of the lines matched, reject
  return False

def prettyPrint(commits):
  """Pretty prints the specified commits with git log

  Respects the GIT_STATS_PRETTY_PRINT environment variable.
  Supported are 'full' and 'oneline', defaults to 'full'.
  """
  
  git = Repo(".").git

  if GIT_STATS_PRETTY_PRINT == "full":
    opts = ["-1", "--name-only"]
    # Enable raw output for the trailing newline, which is desired in this case
    kwargs = { "with_raw_output" : True }
  elif GIT_STATS_PRETTY_PRINT == "oneline":
    opts = ["-1", "--pretty=oneline"]
    kwargs = {}

  for commit in commits:
    # Copy the opts so that we don't have to kick off the commit later on
    thisopts = opts[:]
    thisopts.append(commit)
    result = git.log(*thisopts, **kwargs)

    print(result)

def _makePathsRelative(paths):
  """Helper function that takes a list of paths and makes it relative

  Args:
    paths: The paths to make relative.

  Returns: A new lists in which the paths are relative to the working dir.
  """

  result = []

  git = Repo(".").git

  # Get our 'distance' to the top
  prefix = git.rev_parse("--show-cdup")

  prefix = prefix

  # Make it relative
  for path in paths:
    result.append(os.path.join(prefix, path))

  return result

def pathsTouchedBy(commit, ignore_added_files=True):
  """Returns a list of paths touched by a specific commit.

  Params:
    commit: A commit identifier as accepted by git-log.
    ignore_added_files: When True newly added files are ignored.

  Returns:
    A list of paths touched by the specified commit.
  """

  git = Repo(".").git
  
  result = git.diff_tree("--name-status", "--no-commit-id", "-r", commit)
  log = result.split('\n')

  paths = []

  for line in log:
    # Ignore empty lines
    if len(line.lstrip()) == 0:
      continue

    # Split the status part and the file name
    splitline = line.split('\t')
    if splitline[0] == 'A' and ignore_added_files:
      continue

    paths.append(splitline[1])

  return paths

def commitsThatTouched(paths, relative=False, touched_files={}):
  """Returns a list of commits that touch the specified paths.
  
  Params:
    paths: A list of changed path relative to the current working dir.
    relative: Whether the paths are relative to the current directory.
    touched_files: A dictionary of files and the commits that touched them.

  Returns:
    A list of 40-character SHA's of the touched commits.
  """

  if not paths:
    raise ValueError("No changed paths specified")

  paths = tuple(paths)

  if not paths in touched_files:
    git = Repo(".").git

    result = git.rev_list("HEAD", "--", with_keep_cwd=relative, *paths)
    touched = result.split('\n')
    touched_files[paths] = touched
  else:
    touched = touched_files[paths]

  return touched

def commitTouched(commit):
  """Shows what commit touched the same files as the specified commit
  """

  touched = pathsTouchedBy(commit, True)
  touched = commitsThatTouched(touched)

  prettyPrint(touched)

def commitmsgMatches(commit, regexp):
  """Returns whether the specified commit matches the specified regexp.

  The commit message for the specified commit is searched,
  returns true iff amatching line is found.

  Params:
    commit: The commit whose commit msg is to be searched.
    regexp: The regexp to match against.

  Returns: Whether the commit msg matches the specified regexp.  
  """

  git = Repo(".").git
  
  result = git.cat_file("-p", commit)
  log = result.split('\n')

  # The commit msg begin after the first empty line
  pos = log.index("")
  pos += 1
  log = log[pos:]

  return _logContains(log, regexp)

def getDiff(commit, ignore_whitespace=True, noContext=False):
  """Returns the commit diff for the specified commit

  Params:
    commit: The commit to get the diff for.

  Returns: The commit diff.
  """

  git = Repo(".").git
 
  args = ["-p"]

  if ignore_whitespace:
    args.append("-w")
    
  if noContext:
    args.append("-U0")

  args.append(commit + "^")
  args.append(commit)
  args.append("--")

  # Don't look before we leap, just try with 'hash^'
  result = git.diff_tree(with_exceptions=False, *args)

  # We missed, retry with '--root' instead
  if not result:
    args[-3] = "--root"
    result = git.diff_tree(*args)

  return result


def commitdiffMatches(commit, regexp, raw_diffs={}):
  """Returns whether the commit diff matches the specified regexp.

  Params:
    commit: The commit whose commit diff is to be searched.
    regexp: The regexp to match against.
    raw_diffs: A dictionary with commits and their diffs.

  Returns: Whether the commit diff matches the specified regexp.
  """

  if not commit in raw_diffs:
    result = getDiff(commit, noContext=True)
    diff = result.split('\n')
    raw_diffs[commit] = diff
  else:
    diff = raw_diffs[commit]

  return _logContains(diff, regexp)

def commitList(log_filter=None, diff_filter=None):
  """Returns a list of all commits that match the specified filters

  Args:
    log_filter: If specified, only commits whose log match are included.
    diff_filter: If specified, only commits whose diff match are included.
  """
  
  git = Repo(".").git

  result = git.rev_list("HEAD")
  commits = result.split('\n')

  if not log_filter and not diff_filter:
    return commits

  result = []

  for commit in commits:
    add_it = True

    if log_filter:
      if not commitmsgMatches(commit, log_filter):
        add_it = False

    if diff_filter:
      if not commitdiffMatches(commit, diff_filter):
        add_it = False

    if add_it:
      result.append(commit)

  return result

def _checkOptions(parser, options):
  """Checks the specified options and uses the parser to indicate errors

  Args:
    parser: The parser to use to signal when the options are bad.
    options: The options to check.
  """

  opts = [options.touched, options.log_contains, 
          options.diff_contains, options.all]
  
  if not parse.isUnique(opts, at_least_one=True):
    parser.error("Please choose exactly one mode")

  if not options.touched:
    if options.relative:
      parser.error("-r makes no sense without -t")
  else:
    # Check if the specified file is valid
    try:
      parse.checkFile(value=options.touched, relative=options.relative)
    except OptionValueError, e:
      parser.error(e)      

def dispatch(*args):
  """Dispatches commit related commands
  """

  # Make the help show 'progname commit' instead of just 'progname'
  progname = os.path.basename(sys.argv[0]) + " commit"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)

  parser.add_option(
      "-t", "--touches",
      dest="touched",
      help="show only commits that modify the specified file")

  # Toy option, since we have git log for that
  parser.add_option(
      "-a", "--all",
      action="store_true",
      help="lists all commits on HEAD")

  parser.add_option(
      "-l", "--log-contains",
      help="show only commits of which the log contains the specified regexp")

  parser.add_option(
      "-d", "--diff-contains",
      help="show only commits of which the diff contains the specified regexp")

  parser.add_option(
      "-r", "--relative",
      action="store_true",
      help="paths are relative to the current directory")

  parser.set_default("relative", False)

  (options, args) = parser.parse_args(list(args))

  _checkOptions(parser, options)

  if options.touched:
    result = commitsThatTouched([options.touched], options.relative)

  if options.diff_contains or options.log_contains or options.all:
    result = commitList(diff_filter=options.diff_contains,
                        log_filter=options.log_contains)

  prettyPrint(result)

