#!/usr/bin/env python

import sys
import os

from optparse import OptionParser

from git_stats import testing
from git_stats import config_tests

all_modules = {
    "config" : config_tests
    }

def dispatch(*args):
  """Dispatches test related commands
  """

  progname = os.path.basename(sys.argv[0]) + " test"
  parser = OptionParser(prog=progname)

  parser.add_option(
      "-a", "--all",
      action="store_true",
      help="run tests for all known modules")

  parser.add_option(
      "-m", "--module",
      help="run the tests defined in the specified module")

  parser.add_option(
      "-s", "--single",
      metavar="TEST",
      help="run only the tests for the specified module")

  parser.add_option(
      "-t", "--tests",
      metavar="TESTS",
      action="store_true",
      help="run only the specified tests")

  parser.add_option(
      "-v", "--verbose",
      action="store_true",
      help="enable verbose output")

  if not args:
    args.append("--help")

  options, args = parser.parse_args(list(args))

  tests = []
  modules = []

  # Add all modules we know about
  if options.all:
    tests.extend(all_modules.keys())

  # Add only one test
  if options.single:
    tests.append(options.single)

  # Also add all tests as specified as command arguments
  if options.tests:
    tests.extend(args)

  if options.module:
    try:
      module = __import__("git_stats." + options.module, fromlist=True)
    except ImportError, e:
      parser.error("Could not import module '%s'.\n%s" % (
        options.module, str(e)))

    modules.append(module)

  for test in tests:
    if not test in all_modules:
      parser.error("Unknown module '%s'." % test)

    module = all_modules[test]
    modules.append(module)

  if not modules:
    parser.error("Please specify which modules to test")

  classes = []

  for module in modules:
    module_classes = module.getTestClasses()
    classes.extend(module_classes)

  runner = testing.GitTestRunner()
  res = runner.runTests(options, classes)

  return res

