#!/usr/bin/env python

import os
import sys

from optparse import OptionParser
from git import Repo

from git_stats import commit

def stagedChanges(ignore_added_files=False):
  """Returns the paths to all files that have changes staged.
  When calling this function the current directory should be the root
  of the git directory that should be acted on.

  Params:
    ignore_added_files: When True files that added newly are ignored.

  Returns:
    A list of paths that were changed.
  """

  git = Repo(".").git
  result = git.diff_index("--cached", "--name-status", "HEAD")

  log = result.split('\n')

  changed = []  

  for line in log:
    # Skip the last empty line
    if len(line.lstrip()) == 0:
      continue

    splitline = line.split('\t')

    # Skip files that were freshly added
    if splitline[0] == 'A' and ignore_added_files:
      continue

    changed.append(splitline[1])

  return changed

def touched(show_all):
  """Returns which commits touched the same files as the staged changes.

  Args:
    show_all: When true, all files, even new ones, are searched.

  Returns: If there were no staged changes, None is returned.
    Otherwise the list of matching commits is returned.
  """

  staged = stagedChanges(not show_all)

  # There were no starged changes, return 'None' explicitly
  if not staged:
    return None
  
  # Find all commits that touched the same files and return them
  touched = commit.commitsThatTouched(staged)
  return touched

def dispatch(*args):
  """Dispatches index related commands
  """

  progname = os.path.basename(sys.argv[0]) + " index"

  parser = OptionParser(prog=progname)

  parser.add_option(
      "-a", "--all",
      action="store_true",
      help="search even new files, which usually have no matching commits")

  parser.add_option(
      "-t", "--touched",
      action="store_true",
      help="shows all commits that touch the same paths as the staged changes")

  # Default to True for now, until there is another option
  parser.set_default("touched", True)
  parser.set_default("all", False)

  (options, args) = parser.parse_args(list(args))  

  result = touched(options.all)

  if not result:
    msg = "No changes staged"
    if options.all:
      msg += " or no matching commits"
    else:
      msg += ", no matching commits, or only new files added"
    
    print(msg)
    return

  commit.prettyPrint(result)

