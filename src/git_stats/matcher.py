#!/usr/bin/env python

import difflib
import os
import sys

from optparse import OptionParser
from git_stats import commit
from git_stats import diff
from git_stats import parse

def calculateDiffSize(difference):
  """Calculates the true diff size

  All lines that start with '+\t', '-\t' are counted.
  Lines that are of size 1 are also counted.
  """

  size = 0

  # Take each line and only count if it is part of the diff
  for line in difference:
    if (len(line) == 1 or line[1] == '\t' and
        (line[0] == '+' or line[0] == '-')):
      size += 1

  return size

def findMatch(left, right):
  """Tries to find a match between left and right

  If it is plausible that there is a match the difference is returend.
  Otherwise False is returned.
  """

  # Get the diff and convert it to a usable format
  res = difflib.unified_diff(left, right, n=0, lineterm="")
  res = list(res)

  # Get some sizes for easy calculation
  ressize = calculateDiffSize(res)
  leftsize = len(left)
  rightsize = len(right)

  # The difference is larger than either side
  if ressize > leftsize or ressize > rightsize:
    return False

  # The difference is larger than the average
  if ressize > (leftsize + rightsize)/2:
    return False

  # This is probably a match, return the difference
  return res

def match(target):
  """Tries to find a match between added and removed hunks

  The diff of the specified commit is retreived and it is
  split into hunks. The hunks that were added are compared
  with the hunks that were deleted. If they are similar the
  pair is deemed a match.
  """

  # Retrieve the diff
  result = commit.getDiff(target)
  targetDiff = result.split('\n')

  # And have it parsed, but don't add line numbering to the hunks
  parsedDiffs = diff.parseCommitDiff(targetDiff, number=False)

  # To store the matches in
  result = []

  # Iterate over all the diffs, e.g., take all pairs
  for left in parsedDiffs:
    for right in parsedDiffs:
      # Don't compare with self, that'd always match
      if left == right:
        continue

      # A removal hunk, not interesting as a left side
      # Only interesting when comparing with addition
      if not left.linesAdded:
        continue

      # An add hunk, not interesting as a right side
      # We are interested in this as a left side
      if not right.linesDeleted:
        continue

      # Try to find a match for this pair
      res = findMatch(left.linesAdded, right.linesDeleted)

      # There was no match
      if not res:
        continue

      result.append(left.linesAdded, right.linesAdded, res)

  return result

def dispatch(*args):
  """Dispatches matching related commands
  """

  # Make the help show 'progname commit' instead of just 'progname'
  progname = os.path.basename(sys.argv[0]) + " matcher"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)

  parser.add_option(
      "-m", "--matcher",
      type="commit",
      help="match the chunks of a diff to find code moves")

  parser.set_default("matcher", "HEAD")

  (options, args) = parser.parse_args(list(args))

  if args:
    parser.error("Please specify a commit to analyse")

  # Get the result
  result = match(options.matcher)

  # And print it
  for line in result:
    print(line)

