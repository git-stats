#!/usr/bin/env python

import os
import sys

from optparse import OptionParser
from git import Repo

from git_stats import parse

def prettyName(branch, pretty_names={}):
  """Looks up the name of a branch and returns it
  
  Returns only the last part of the branchname.

  Args:
    branch: The branch to print
    pretty_names: A dictionary with commits and their names
  """

  if not branch in pretty_names:
    git = Repo(".").git
    name = git.describe("--all", branch)
    splitname = name.split('/')
    name = splitname[-1]
    pretty_names[branch] = name
  else:
    name = pretty_names[branch]

  return name

class Metric:
  """This class represents a metric
  
  It is meant to store arbitrary data related to a specific metric.
  """

  def __init__(self):
    self.branch = ""
    self.commit = ""
    self.dilution = ""

  def __str__(self):
    res = ("Branch %s, dilution: %d." %
        (prettyName(self.branch), self.dilution))

    return res

def _bestMetric(metrics):
  """Finds the best metric from a set

  Iterates the metrics and selects the ones with the lowest dilution.
  All the returned metrics are guaranteed to have the same dilution.

  Args:
    metrics: The metrics to pick the best ones from.

  Returns: A list with the best metrics.
  """

  champs = []

  # Find the best metric
  for metric in metrics:
    # Store the current value for easy access
    dilution = metric.dilution

    # First element, set the minimum to the current
    if not champs:
      min = dilution

    # Worse than our best, not interesting
    if dilution > min:
      continue

    # Clear the old ones if we have a better minimum
    if dilution < min:
      champs = []
      min = dilution

    champs.append(metric)

  return min, champs

def _calculateDilution(target, start, parent_lists, global_memory, ignore=[]):
  """Calculates the dilution for the specified commit.

  Args:
    target: The commit to calculate the dilution for.
    start: The initial metric information.
    parent_list: A dictionary with commits and their parents.
    global_memory: A dictionary with all commits and their parents.
    ignore: The parent commits of the target commit which should be ignored.
  """

  metrics = []

  memory = {}
  stack = []  
  stack.append(start)

  git = Repo(".").git

  while stack:
    current = stack.pop()

    # We found what we are looking for
    if target == current.commit:
      metrics.append(current)
      continue

    # Ignore commits that are past the target
    if current.commit in ignore:
      continue

    # If we already checked this commit, don't add it again
    if current.commit in memory:
      dilution = memory[current.commit]
      if dilution <= current.dilution:
        continue

    # Nor when already checked in a better branch
    if current.commit in global_memory:
      dilution = global_memory[current.commit]
      if dilution < current.dilution:
        continue

    # Remember this commit
    memory[current.commit] = current.dilution
    global_memory[current.commit] = current.dilution

    # Defaulting to None in the case we only have a partial parent listing
    parents = parent_lists.get(current.commit, None)

    # Root commit, we can't go any further
    if not parents:
      continue

    # Check all the parents and give only the first one 0 extra dilution
    for parent in parents:
      if parent == parents[0]:
        newdilution = current.dilution
      else:
        newdilution = current.dilution + 1

      # Create the next target
      next = Metric()
      next.commit = parent
      next.branch = current.branch
      next.dilution = newdilution

      # Add it to the stack to be examined
      stack.append(next)

  # Nothing new was found here
  if not metrics:
    return []

  # Return only the best metric
  min, champs = _bestMetric(metrics)
  return champs

def debugPrint(debug, text):
  """Only print the specified text if debug is on

  debug: Whether debug is on.
  text: The text to print
  """

  if debug:
    print(text)

def getBranchMap(branches):
  """Creates a branchMap from the specified branches and returns it

  The hash of each branch is retreived and stored in a
  dictionary with the condition that all values in the map
  are unique. As such if multiple branches map to one hash,
  only one of those branches (the first one) will be in the
  returned map.

  Args:
    branches: A list of branches that should be in the map.

  Returns: A dictionary with branch names and their hashes.
  """

  git = Repo(".").git

  branch_map = {}

  for branch in branches:
    # Retreive the hash of each branch
    branch_hash = git.rev_parse(branch)

    if branch_hash in branch_map.values():
      continue
    
    branch_map[branch] = branch_hash

  return branch_map
  
def getParentList(commits):
  """Retreives the parents of the specified commits and their parents

  The parents for all the specified commits, and for the
  parents of those commits, are retreived and stored in
  a dictionary.

  Args:
    commits: A list of commits for which the parents should be retreived.

  Returns: A dictionary with commits and their parents.
  """

  git = Repo(".").git

  # First mine some data
  result = git.rev_list("--parents", *commits)
  data = result.split('\n')

  parent_list = {}

  for item in data:
    # Each line contains the commit first, and then all it's parents
    splitline = item.split(' ')
    first = splitline[0]

    # This commit has no parents at all
    if len(splitline) == 1:
      parent_list[first] = []
    #  This commits has no parents either
    elif splitline[1] == '':
      parent_list[first] = []
    # Store all the parents
    else:
      parent_list[first] = splitline[1:]

  return parent_list

def belongsTo(commit,
              branch_map,
              parent_list,
              pretty_names={},
              debug=False,
              ignore_parents=False):
  """Returns which branches the specified commit belongs to

  Args:
    commit: The commit to examine.
    branch_map: A dictionary of all refs and their hashes.
    parent_list: A dictionary with tuples of refs and their rev-lists.
    pretty_names: A dictionary with commits and their names
    debug: Whether to return debug information as well.

  Returns: A list of branches that the commit belongs to.
  """

  git = Repo(".").git

  if commit in branch_map:
    debugPrint(debug, "Easy as pie, that's a branch head!")
    name = branch_map[commit]
    return [name]

  metrics = []

  # Retreive the parents of the target commit and ignore them
  if ignore_parents:
    result = git.rev_list(commit)
    ignore = set(result.split('\n'))
  else:
    ignore = []

  debugPrint(debug, "Checking branches now:")

  memory = {}

  # Collect the metric for all branches
  for branch_name, branch_hash in branch_map.iteritems():
    debugPrint(debug, branch_name)
    
    # Create the first metric
    metric = Metric()
    metric.branch = branch_hash
    metric.commit = branch_hash
    metric.dilution = 0

    # Find the dilution for this branch
    metric = _calculateDilution(commit, metric, parent_list, memory, ignore)
    metrics = metrics + metric

  debugPrint(debug, "Done.\n")

  if debug:
    print("Listing found metrics:")
    for metric in metrics:
      print(metric)

  min, champs = _bestMetric(metrics)

  debugPrint(debug, "Done.\n")

  results = []

  if debug:
    results.append("The minimal dilution is: " + str(min))

  # Loop over all the champs and get their name
  for metric in champs:
    results.append(prettyName(metric.branch, pretty_names))

  return results

def branchcontains(branch, commit):
  """returns whether the specified branch contains the specified commit.

  params:
    branch: the branch.
    commit: the commit.

  returns:
    whether the branch contains the commit.
  """

  git = git(".")
  arg = branch + ".." + commit
  result = git.rev_list(arg)

  if result:
    # if there is a difference between these sets, the commit is not in the branch
    return false
  else:
    # there is no difference between the two, thus the branch contains the commit
    return true

def branchList(commit_filter, include_remotes=False):
  """Returns all branches that contain the specified commit

  Args:
    commit_filter: The commit to filter by.
    include_remotes: Whether to search remote branches as well.
  """
  
  git = Repo(".").git

  args = []

  if include_remotes:
    args.append("-a")

  if commit_filter:
    args.append("--contains")
    args.append(commit_filter)

  result = git.branch(*args)

  branches = result.split('\n')

  result = []

  for branch in branches:
    result.append(branch[2:])

  return result

def dispatch(*args):
  """Dispatches branch related commands
  """

  progname = os.path.basename(sys.argv[0]) + " branch"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)


  parser.add_option(
      "-b", "--belongs-to",
      type="commit",
      metavar="COMMIT",
      help="find out which branch the specified commit belongs to")

  parser.add_option(
      "-d", "--debug",
      action="store_true",
      help="print debug information on the found metrics")

  parser.add_option(
      "-c", "--contains",
      type="commit",
      help="show only branches that contain the specified commit")

  parser.add_option(
      "-r", "--remotes",
      action="store_true",
      help="include remotes in the listing")

  parser.set_default("debug", False)
  parser.set_default("remotes", False)

  (options, args) = parser.parse_args(list(args))

  if options.belongs_to:
    branches = branchList(options.belongs_to, include_remotes=options.remotes)

    branch_map = getBranchMap(branches)
    parent_list = getParentList(branch_map.values())

    result = belongsTo( options.belongs_to,
                        branch_map,
                        parent_list,
                        debug=options.debug)
  else:
    result = branchList(commit_filter=options.contains,
                        include_remotes=options.remotes)

  print("Matching branches:")

  for branch in result:
    print(branch)

