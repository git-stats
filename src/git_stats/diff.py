#!/usr/bin/env python

import collections
import os
import sys

from optparse import OptionParser

from git_stats import commit
from git_stats import parse

class FileDiff:
  """A class to store the information of a file diff in.

  Fields:
    afile: The file used as the left side of the diff.
    bfile: The file used as the right side of the diff.
    context: The context of this diff.
    apos: Where the left side of the diff starts.
    bpos: Where the right side of the diff starts.
    linesAdded: Which lines were added.
    linesDeleted: Which lines were deleted.
  """

  def __init__(self, diffHeader):
    self.afile = ""
    self.bfile = ""
    self.context = ""

    self.linesAdded = []
    self.linesDeleted = []

    for line in diffHeader:
      if line.startswith("--- "):
        self.afile = line[4:]

      if line.startswith("+++ "):
        self.bfile = line[4:]

  def __str__(self):
    str = ("Diff for '%s' (%d) against '%s' (%d)\n%s\n%s\n%s" %
        (self.afile, self.astart, self.bfile, self.bstart,
          self.context, self.linesAdded, self.linesDeleted))

    return str

def _splitDiff(diff):
  """Splits off the diff in chunks, one for each file

  Params:
    diff: The diff to split up.

  Returns: A list containing a chunk per file.
  """

  chunks = []

  chunk = []

  for line in diff:
    # Start of a new chunk, add the old one if there is one
    if line.startswith("diff"):
      if chunk:
        chunks.append(chunk)

      chunk = []

    chunk.append(line)

  # Add the last one
  if chunk:
    chunks.append(chunk)

  return chunks

def _splitFileDiff(diff):
  """Splits a file diff into chunks, one per area.

  Params:
    diff: The diff to split up.

  Returns: The diff header and a list with all the chunks.
  """

  chunks = []

  header = []
  chunk = []

  start = len(diff)

  # Find out where the header stops
  for i, line in enumerate(diff):
    if line.startswith("@@"):
      # Start at the first hunk, which is this line
      start = i
      break

    header.append(line)

  # Chop off the header and split up the chunks
  for line in diff[start:]:
    # Start a new chunk and add the old one if there is one
    if line.startswith("@@"):
      if chunk:
        chunks.append(chunk)

      chunk = []

    chunk.append(line)

  # Add the last one
  if chunk:
    chunks.append(chunk)

  return header, chunks

def _parseFileDiff(header, chunk, number=True):
  """Takes a file diff and returns the parsed result

  Params:
    header: The diff header.
    chunk: The chunk to parse.
    number: Whether to number the line differences.

  Returns: A FileDiff containing the parsed diff.
  """

  result = FileDiff(header)

  # Empty diff, no need to do anything
  if not chunk:
    return result

  deleted = []
  added = []

  # Find out where the context line ends, skipping the first '@@'
  to = chunk[0].find("@@", 2)

  # Get the context, skipping the first and last '@@"
  context = chunk[0][3:to]

  # Split it at the spaces and store the positions, ignoring '-' and '+'
  split = context.split(' ')
  a = split[0][1:]
  b = split[1][1:]

  apos = int(a.split(',')[0])
  bpos = int(b.split(',')[0])

  result.astart = apos
  result.bstart = bpos

  # Start at the first line (skip the context line)
  for line in chunk[1:]:
    if line.startswith("-"):
      if number:
        deleted.append((apos, line[1:]))
      else:
        deleted.append(line[1:])
      apos += 1

    if line.startswith("+"):
      if number:
        added.append((bpos, line[1:]))
      else:
        added.append(line[1:])
      bpos += 1

  result.linesDeleted = deleted
  result.linesAdded = added
  result.context = context

  return result

def parseCommitDiff(diff, number=True):
  """Takes a commit diff and returns the parsed result

  Params:
    diff: The diff to parse.

  Returns: A parsedDiff instance containing the parsed diff.
  """

  result = []

  # Split the diff in file sized chunks
  chunks = _splitDiff(diff)

  # Loop over all the file diffs and parse them
  for chunk in chunks:
    header, filechunks = _splitFileDiff(chunk)
    
    # Loop over all the chunks and parse them
    for filechunk in filechunks:
      # Get the result and store it
      fd = _parseFileDiff(header, filechunk, number)
      result.append(fd)

  return result

def _compareFileDiffs(adiff, bdiff, invert=False):
  """Compares two FileDiffs and returns whether they are equal

  Args:
    adiff: The first FileDiff.
    bdiff: The second FileDiff.
    invert: Whether to compare linesAdded with linesDeleted.

  Returns: Whether the two diffs are equal.
  """

  if invert:
    # Cross compare added with deleted
    if not adiff.linesAdded == bdiff.linesDeleted:
      return False
    # And vise versa
    if not adiff.linesDeleted == bdiff.linesAdded:
        return False
  else:
    # Do a normal comparison between added lines
    if not adiff.linesAdded == bdiff.linesAdded:
      return False
    # And between the deleted lines
    if not adiff.linesDeleted == bdiff.linesDeleted:
      return False
  
  # Checked everything, accept
  return True

def _compareDiffs(adiffs, bdiffs, compareChanges=False, invert=False):
  """Compares the two diffs and returns whether they are equal

  Args:
    adiffs: The first set of diffs.
    bdiffs: The second set of diffs.
    compareChanges: Whether to compare not only which lines changed.
    invert: When compareChanges, invert the comparison of deleted/added.

  Returns: Whether the diffs are equal.
  """

  for fd in adiffs:
    # Look for a match in the bdiffs
    for theirs in bdiffs:

      # Check for empty diffs
      if (((not fd.linesAdded and not fd.linesDeleted) and
          (theirs.linesAdded or theirs.linesDeleted)) or
          ((not theirs.linesAdded and not theirs.linesDeleted) and
          (fd.linesAdded and fd.linesDeleted))):
            return False

      # Check if both are empty diffs
      if (not fd.linesAdded and not theirs.linesAdded and
          not fd.linesDeleted and not theirs.linesDeleted):
            return True

      # Looks like we have a match
      if ((theirs.astart <= fd.astart and theirs.bstart >= fd.bstart) or
        (invert and theirs.astart <= fd.bstart and theirs.bstart >= fd.astart)):

       # If we want to compare changes, do they match
        if compareChanges:
          # Reject if they are inequal
          if not _compareFileDiffs(fd, theirs, invert):
            return False

        # It was indeed a match, stop searching through bdiffs
        break
      
    else:
      # Went through all items in bdiffs and couldn't find a matching pair 
      return False
      
  # All items in adiffs checked, all had a matching pair, accept.
  return True

def _difference(adiffs, bdiffs, compareChanges=False, invert=False):
  """Calculates the difference between two diffs and returns it

  Params:
    adiffs: The first set of diffs.
    bdiffs: The second set of diffs.
    compareChanges: Whether to compare not only which lines changed.
    invert: When compareChanges, invert the comparison of deleted/added.

  Returns: Which keys are missing and the difference between both diffs.
  """

  afiles = collections.defaultdict(list)
  bfiles = collections.defaultdict(list)

  missing = []
  difference = []

  # Group the diffs by file pair
  for fd in adiffs:
    afiles[(fd.afile, fd.bfile)].append(fd)

  # Group the diffs by file pair
  for fd in bdiffs:
    bfiles[(fd.afile, fd.bfile)].append(fd)

  # Examine all the diffs and see if they match
  for key, fds in afiles.iteritems():
    # There is no counterpart for this file, record that
    if not key in bfiles:
      missing.append(key)
      continue

    theirs = bfiles[key]

    # Compare the diffs, if not equal record that
    if not _compareDiffs(fds, theirs, compareChanges, invert):
      difference.append((fds, theirs))
  
  return missing, difference

def _getParsedDiff(target, raw_diffs, parsed_diffs):
  """Retrieves a parsed commit diff for the specified file

  Args:
    target: The commit to get the diff for.
    raw_diffs: A dictionary with commits and their raw diffs.
    parsed_diffs: A dictionary with commits and their parsed diffs.
  """

  if not target in parsed_diffs:
    # Get the diff, but ignore whitespace
    if not target in raw_diffs:
      result = commit.getDiff(target, noContext=True)
      diffTarget = result.split('\n')
    else:
      diffTarget = raw_diffs[target]

    parsedTarget = parseCommitDiff(diffTarget)
    raw_diffs[target] = diffTarget
    parsed_diffs[target] = parsedTarget
  else:
    parsedTarget = parsed_diffs[target]

  return parsedTarget

def commitdiffEqual(original,
                    potentialMatch,
                    threshold=0,
                    compareChanges=True,
                    invert=False,
                    verbose=True,
                    raw_diffs={},
                    parsed_diffs={}):
  """Tests whether a commit matches another by a specified threshhold.

  Params:
    original: The original commit that is to be checked.
    potentialMatch: The commit that might match original.
    threshhold: The threshold for how close they have to match.
    compareChanges: Whether to compare the changes made or just changes lines.
    invert: Whether to compare deletions with insertions instead.
    raw_diffs: A dictionary of commits adn their raw diffs.
    parsed_diffs: A dictionary of commits and their parsed diffs.

  Returns: Whether the commit diffs are equal.
  """

  # Retrieved the parsed diffs
  parsedOriginal =        _getParsedDiff( original,
                                          raw_diffs,
                                          parsed_diffs)

  parsedPotentialMatch =  _getParsedDiff( potentialMatch,
                                          raw_diffs,
                                          parsed_diffs)

  # Get the difference between both
  missing, diff = _difference(parsedOriginal,
                              parsedPotentialMatch,
                              compareChanges=compareChanges,
                              invert=invert)

  if verbose:
    if missing:
      print("Missing the following keys:")
      for key in missing:
        print(key)

    if diff:
      print("Found the following differences:")
      for ours, theirs in diff:
        print("---")
        for fd in ours:
          print(fd)
        print("\nDoes not match:\n")
        for fd in theirs:
          print(fd)
        print("----")

  # TODO use threshhold

  # Unequal if something missing, or there is a difference
  return not (missing or diff)

def isReverted(commit, potential_revert, raw_diffs={}, parsed_diffs={}):
  """Returns whether the specified commit is reverted by another one

  Args:
    commit: The commit that might be reverted.
    potential_revert: The commit that might be a revert.
    raw_diffs: A dictionary of commits adn their raw diffs.
    parsed_diffs: A dictionary of commits and their parsed diffs.
  """  

  return commitdiffEqual( commit,
                          potential_revert,
                          invert=True,
                          verbose=False,
                          raw_diffs=raw_diffs,
                          parsed_diffs=parsed_diffs)

def findReverts(potential_revert,
                raw_diffs={},
                parsed_diffs={},
                touched_files={}):
  """Returns all commits that are reverted by the specified commit

  Args:
    potential_revert: The commit to check for reverts for.
    raw_diffs: A dictionary with commits and their raw diffs.
    parsed_diffs: A dictionary with commits and their parsed diffs.
    touched_files: A dictionary with files and the commits that touched them.
  """

  # Find out what paths this commit touched
  paths = commit.pathsTouchedBy(potential_revert)
  
  # If no paths were touched, there can't be any reverts
  if not paths:
    return []

  # Retrieve all commits that touched the same paths
  commits = commit.commitsThatTouched(paths, touched_files=touched_files)

  result = []

  # Check all the found commits to see if they are a revert
  for aCommit in commits:
    # Don't compare to self
    if aCommit == potential_revert:
      continue

    if isReverted(aCommit,
                  potential_revert,
                  raw_diffs=raw_diffs,
                  parsed_diffs=parsed_diffs):
      result.append(aCommit)

  return result

def dispatch(*args):
  """Dispatches diff related commands
  """

  progname = os.path.basename(sys.argv[0]) + " diff"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)
  
  parser.add_option(
      "-e", "--equals",
      type="commit",
      nargs=2,
      help="show whether the two diffs for the specified commits match",
      metavar="COMMIT COMMIT")

  parser.add_option(
      "-t", "--threshold",
      type="int",
      help="the threshold for comparison")

  parser.add_option(
      "-n", "--no-compare",
      action="store_false",
      dest="compare",
      help="do not compare the diff content, just look at which lines were touched")

  parser.add_option(
      "-i", "--invert",
      action="store_true",
      help="compare additions with deletions instead of with additions, and vise versa")

  parser.add_option(
      "-r", "--reverts",
      type="commit",
      help="show only commits that are reverted by the specified commit")

  parser.set_default("threshold", 0)
  parser.set_default("compare", True)
  parser.set_default("invert", False)

  (options, args) = parser.parse_args(list(args))

  if options.equals:
    result = commitdiffEqual( threshold=options.threshold, 
                              compareChanges=options.compare, 
                              invert=options.invert, 
                              *options.equals)

    if result:
      print("Equal")

  if options.reverts:
    result = findReverts(options.reverts)
    commit.prettyPrint(result)

