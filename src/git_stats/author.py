#!/usr/bin/env python

import operator
import os
import sys

from optparse import OptionParser, OptionValueError
from git import Repo

from git_stats import parse

class Activity:
  """Simple storage class containing stats on the activity in one file."""

  def __init__(self):
    self.count = 0
    self.added = 0
    self.deleted = 0
    self.id = []

  def __str__(self):
    return ("%4d: %5d+ %5d- %5d~" %
        (self.count, self.added, self.deleted, self.added-self.deleted))

def activityInArea(log):
  """Parses the specified file containing commit logs.
  The output is expected to be in the format described below:
  [
    [<id>\n]+
    [<lines added>\t<lines deleted>\t<path>]+
    \n
  ]+

  Params:
    log: The log formatted as described above.

  Returns:
    A dictionary containing activity per author is returned.
    Each author contains a dictionary with activity per path.
    Each path contains one Activity object with the aggregated results.
  """

  # Create a dict to store the activity stats per path
  activityByAuthor = {}

  # Create a place to store the result in
  activities = []
  ids = []

  i = 0

  # Parse all the lines in the file
  for line in log:
    i += 1

    # Split the line at the tab and store the data
    splitline = line.split('\t')
    size = len(splitline)
    length = len(line.lstrip())

    # There is something on this line, but it contains no separator
    if size == 1 and length > 0:
      # Get the id address minus the newline
      ids.append(line[:-1])
    elif size == 3:
      activity = Activity()
      activity.id = ids

      try:
        addpart = splitline[0]
        deletepart = splitline[1]

        if addpart == '-':
          addpart = 0

        if deletepart == '-':
          deletepart = 0

        activity.added = int(addpart)
        activity.deleted = int(deletepart)
      except ValueError, e:
        print("On line '%d', could not convert number: %s" % (i,str(e)))

      activity.path = splitline[2][:-1]
      activities.append(activity)
    elif length == 0:
      for activity in activities:
        for author in activity.id:
          if not author in activityByAuthor:
            activityByAuthor[author] = {}

          activityByPath = activityByAuthor[author]

          # If we have not encountered this path, create an entry
          if not activity.path in activityByPath:
            addme = Activity()
            activityByPath[activity.path] = addme

          known = activityByPath[activity.path]
          
          result = Activity()
          result.added = known.added + activity.added
          result.deleted = known.deleted + activity.deleted
          result.count = known.count + 1
          
          # Store it under it's path
          activityByPath[activity.path] = result

          # Store it under it's author
          activityByAuthor[author] = activityByPath

      # Create a fresh activity to store the next round in
      activities = []
      ids = []
    else:
      print("Cannot parse line %d." % i)

  # Return the result
  return activityByAuthor

def activityInFile(path, id, start_from, relative):
  """Shows the activity for the file in the current repo.

  Params:
    path: The path to filter on.
    id: The id of the developer to show in the result.
    startfrom: The commit to start logging from.
    relative: Treat path as relative to the current working directory.
 """

  git = Repo(".").git

  result = git.log(start_from, "--", path, pretty="format:%" + id, with_keep_cwd=relative)
  activity = result.split('\n')

  result = {}

  for line in activity:
    # Create an entry if there was none for this author yet
    if not line in result:
      result[line] = 0

    result[line] += 1

  return result

def activity(id, field, start_from):
  """Shows the activity for the specified developer in the current repo.

  Params:
    id: The id of the developer, as specified by field.
    field: The field to filter on.
    startfrom: The commit to start logging from.
  """

  git = Repo(".").git
  result = git.log("--numstat", start_from, "--", pretty="format:%" + field)

  log = result.splitlines(True)
  allActivity = activityInArea(log)

  result = []

  if not id:
    for author in sorted(allActivity):
      activity = allActivity[author]
      result.append(author + ":")

      for key in sorted(activity):
        value = activity[key]
        result.append("\t%s = %s" % (str(value), str(key)))

      result.append("")

    return result

  if not id in allActivity:
    result.append("Unknown author " + id)
    result.append("Known authors:")
    result.extend(allActivity.keys())
    
    return result

  activity = allActivity[id]

  for key in sorted(activity):
    value = activity[key]
    result.append("%s = %s" % (str(value), str(key)))

  return result

def aggregateActivity(id_filter, field, start_from):
  """Aggregates the activity for all developers

  Args:
    id_filter: The id to filter on, if None all developers will be shown.
    field: The field to filter on.
  """

  git = Repo(".").git
  result = git.log("--numstat", start_from, "--", pretty="format:%" + field)

  log = result.splitlines(True)
  allActivity = activityInArea(log)

  aggregatedActivity = {}

  for _, activityByPath in allActivity.iteritems():
    for path, activity in activityByPath.iteritems():
      if not path in aggregatedActivity:
        aggregatedActivity[path] = Activity()

      known = aggregatedActivity[path]

      result = Activity()
      result.added = known.added + activity.added
      result.deleted = known.deleted + activity.deleted
      result.count = known.count + activity.count

      aggregatedActivity[path] = result
 
  result = []

  for key in sorted(aggregatedActivity):
    value = aggregatedActivity[key]
    result.append("%s = %s" % (str(value), str(key)))

  return result

def _checkOptions(parser, options):
  """Checks the specified options and uses the parser to indicate errors

  Args:
    parser: The parser to use to signal when the options are bad.
    options: The options to check.
  """

  opts = [options.aggregate, options.developer, options.file, options.everyone]

  if not parse.isUnique(opts, at_least_one=True):
    parser.error("Please choose exactly one mode")

  if options.file:
    try:
      parse.checkFile(value=options.file, relative=options.relative)
    except OptionValueError, e:
      parser.error(e)      


def dispatch(*args):
  """Dispatches author related commands
  """

  progname = os.path.basename(sys.argv[0]) + " author"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)

  parser.add_option(
      "-a", "--aggregate",
      action="store_true",
      help="aggregate the results")

  parser.add_option(
      "-e", "--everyone",
      action="store_true",
      help="show the activity of all developers")

  parser.add_option(
      "-d", "--developer",
      help="the id to filter on")

  parser.add_option(
      "-f", "--file",
      help="the file to filter on")

  parser.add_option(
      "-i", "--id",
      help="the one/two letter identifier specifying which field to use as id")

  parser.add_option(
      "-s", "--start-from",
      type="commit",
      metavar="COMMIT",
      help="the commit to start logging from")

  parser.add_option(
      "-r", "--relative",
      action="store_true",
      help="paths are relative to the current directory")

  parser.set_default("id", "ae")
  parser.set_default("start_from", "HEAD")
  parser.set_default("relative", False)

  (options, args) = parser.parse_args(list(args))

  _checkOptions(parser, options)

  if options.aggregate:
    result = aggregateActivity(options.developer, options.id, options.start_from)
  elif options.developer or options.everyone:
    result = activity(options.developer, options.id, options.start_from)
  elif options.file:
    activity_for_file = activityInFile( options.file,
                                        options.id,
                                        options.start_from,
                                        options.relative)

    result = []

    for key, value in sorted(activity_for_file.iteritems(), reverse=True,
                             key=operator.itemgetter(1)):
      result.append("%s: %s" % (key, str(value)))

  for line in result:
    print(line)

