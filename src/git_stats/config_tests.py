#!/usr/bin/env python

import os
import unittest

from git_stats import config

def getTestClasses():
  """
  """

  return [
      TestRead,
      TestGetDefaultConfigPaths,
      TestReadConfigPaths,
      ]

class TestRead(unittest.TestCase):
  """Tests the read function of the config module
  """

  def setUp(self):
    self.lines = [
        "[core]",
        "   foo = bar",
        "   camelCaseCoreSetting = eggs",
        "[GitStats]",
        "   setting1 = value1",
        "   camelCaseSetting = camelCaseValue"
        "",
        ]

    self.empty = []

    self.empty_config = [
        "[GitStats]",
        ]

    self.multiple_empty_configs = [
        "[GitStats]",
        "[GitStats]",
        "[core]",
        "foo = bar",
        "",
        "[GitStats]",
        ]

    self.multiple_configs = [
        "",
        "[GitStats]",
        "   setting0 = value0",
        "   setting1 = value1",
        "   setting2 = value2",
        "",
        "[core]",
        "   core_setting = foo",
        "[GitStats]",
        "   setting3 = value3",
        "[color]",
        "   color_setting = bar",
        "[GitStats]",
        "   setting4 = value4",
        ]

  def testNoConvert(self):
    """Test the readConfig method with no converting
    """

    result = config.read(self.lines, convert_camel_case=False)

    self.failUnlessEqual(
        "value1",
        result["setting1"],
        "Regular setting not properly parsed")

    self.failUnlessEqual(
        "camelCaseValue",
        result["camelCaseSetting"],
        "Camel case setting not properly parsed")

    self.failIf(
        "foo" in result,
        "Non GitStats setting was parsed")

    self.failIf(
        "camelCaseCoreSetting" in result,
        "Camel case non GitStats setting was parsed")

  def testWithConvert(self):
    """Test the readConfig method with converting enabled
    """

    result = config.read(self.lines, convert_camel_case=True)

    self.failUnlessEqual(
        "value1",
        result["setting1"],
        "Regular setting not properly parsed")

    self.failUnlessEqual(
        "camelCaseValue",
        result["camel_case_setting"],
        "Camel case setting not properly parsed")

    self.failIf(
        "foo" in result,
        "Non GitStats setting was parsed")

    self.failIf(
        "camelCaseCoreSetting" in result,
        "Camel case non GitStats setting was parsed")

  def testEmpty(self):
    """Test the readConfig method with no contents
    """

    result = config.read(self.empty)

    self.failIf(
        result,
        "Empty config did not yield an empty result")

  def testEmptyConfig(self):
    """Test the readConfig method with an empty config
    """

    result = config.read(self.empty_config)

    self.failIf(
        result,
        "Empty config did not yield an empty result")

  def testMultipleEmptyConfigs(self):
    """Test the readConfig method with multiple empty configs
    """

    result = config.read(self.multiple_empty_configs)

    self.failIf(
        result,
        "Multiple empty configs did not yield an empty result")

  def testMultipleConfigs(self):
    """Test the readConfig method with multiple verses
    """

    result = config.read(self.multiple_configs)

    self.failUnlessEqual(
        5,
        len(result),
        "Parsed result did not contain 5 values")

    for i in range(5):
      self.failUnlessEqual(
          "value%d" % i,
          result["setting%d" % i],
          "Config value for setting%d did not match value%d." % (i,i))

class TestGetDefaultConfigPaths(unittest.TestCase):
  """Tests the getDefaultConfigPaths method of the config module
  """

  def setUp(self):
    self.result = config._getDefaultConfigPaths()

  def testContainsConfig(self):
    """Tests that the plain "config" file is included
    """

    self.failUnless(
        "config" in self.result,
        "The plain 'config' file is not in the default paths")

  def testContainsGitConfig(self):
    """Test that the .git/config file is included
    """

    self.failUnless(
         any(s.endswith('.git/config') for s in self.result),
        "The ./git/config file is not in the default paths")

  def testContainsGitconfig(self):
    """Test that the .gitconfig file is included
    """

    self.failUnless(
        any(s.endswith('.gitconfig') for s in self.result),
        "The .gitconfig file is not in the default paths")

class TestReadConfigPaths(unittest.TestCase):
  """Test the readConfigPaths method of the config module
  """

  def setUp(self):
    lines = [
        "[GitStats]\n",
        "   option1 = value1\n",
        "   option2 = value2\n",
        ]

    file = open("config1", "w")
    file.writelines(lines)

    lines = [
        "[GitStats]\n",
        "   option2 = anothervalue2\n",
        "   option3 = anothervalue3\n",
        ]

    file = open("config2", "w")
    file.writelines(lines)

  def tearDown(self):
    os.remove("config1")
    os.remove("config2")

  def testReadMultipleValues(self):
    """Tests if all values were read in
    """

    paths = ["config1", "config2"]

    result = config._readConfigPaths(paths, False)

    self.failUnlessEqual(
        "value1",
        result["option1"],
        "Value of 'option1', present in only one file, not read in properly")

    self.failIfEqual(
        "value2",
        result["option2"],
        "Value of 'option2' not properly overriden by second file")

    self.failUnlessEqual(
        "anothervalue2",
        result["option2"],
        "Value of 'option2' not retreived properly")

    self.failUnlessEqual(
        "anothervalue3",
        result["option3"],
        "Value of 'option3', present in only one file, not read in properly")

