#!/usr/bin/env python

import copy

from git import Repo
from optparse import OptionParser, Option, OptionValueError

def _checkFile(option, opt, value):
  """Checks whether value is a valid file and returns it if so
  """

  return checkFile(value)


def checkFile(value, relative=False):
  """Check whether value is a valid file and returns it if so

  Args:
    value: The value to check.
    relative: Whether to treat value as a path relative to the working dir.
  """

  git = Repo(".").git

  file = git.ls_files("--full-name", "--", value, with_keep_cwd=relative)

  if not file:
    raise OptionValueError("Unknown file '%s'" % value)

  splitfile = file.split('\n')

  if len(splitfile) > 1:
    raise OptionValueError(
        "Please specify only one file, '%s' matches more than one" % value)

  return splitfile[0]

def _check_commit(option, opt, value):
  """Checks whether value is a valid refspec and returns its hash if so
  """

  git = Repo(".").git
  rev = git.rev_parse("--verify", value, with_exceptions=False)

  if not rev:
    raise OptionValueError("Unknown commit '%s'" % value)

  return rev

def _check_bool(option, opt, value):
  """Checks whether a value is a boolean and returns it's value

  Understands about 'True', 'False', and 'None'.
  """

  if value == "True":
    return True

  if value == "False":
    return False

  if value == "None":
    return None

  raise OptionValueError("Not a boolean: '%s'" % value)

class GitOption(Option):
  """This parser understands three new types:
  commit: A commit must specify exactly 1 commit in the current repository.
  file: A file must specify exactly 1 tracked file in the current repository.
  bool: A bool must be 'True', 'False', or 'None'.
  """

  # Define the new types
  TYPES = Option.TYPES + ("commit",) + ("file",) + ("bool",)

  # Copy the type checker list so that we don't keep the default ones
  TYPE_CHECKER = copy.copy(Option.TYPE_CHECKER)

  # Add our own options
  TYPE_CHECKER["commit"] = _check_commit
  TYPE_CHECKER["file"] = _checkFile
  TYPE_CHECKER["bool"] = _check_bool

def isUnique(options, at_least_one=False):
  """Checks if a list of options is unique

  Args:
    options: The list of options to check
    at_least_one: If set, when no optiosn are set, return False
  """

  unique = False

  for option in options:
    if option:
      # If we already found one, it's not unique for sure
      if unique:
        return False
      else:
        unique = True

  # If there is only one, it's unique
  if unique:
    return True

  # None found, so unique only if we don't require at least one
  return not at_least_one

