#!/usr/bin/env python

import os
import sys

from optparse import OptionParser, OptionValueError
from git import Repo

from git_stats import branch
from git_stats import commit
from git_stats import config
from git_stats import diff
from git_stats import parse

options_list = [
    "branch_filter",
    "branch_filter_rating",
    "debug",
    "diff_filter",
    "diff_filter_rating",
    "msg_filter",
    "msg_filter_rating",
    "ignore_parents",
    "limit",
    "revert_rating",
    ]

class CommitInfo:
  """Contains information about a specific commit
  """

  def __init__(self, commit, options):
    """Initializes with the specified commit
    """

    self.options = options
    self.commit = commit
    self.branches = []
    self.reverts = []
    self.msg_matches = ""
    self.diff_matches = ""

  def isBugfix(self):
    """Checks if this commit is a bugfix

    Returns: Whether branches, reverts, msg_matches or diff_matches is set.
    """

    return (self.branches
        or self.reverts
        or self.msg_matches
        or self.diff_matches)

  def bugfixRating(self):
    """Calculates the bugfix rating for this commit
    """

    score = 0

    if self.branches and getattr(self.options, "branch_filter_rating", None):
      score += self.options.branch_filter_rating

    if self.reverts and getattr(self.options, "reverts_rating", None):
      score += self.options.reverts_rating

    if self.msg_matches and getattr(self.options, "msg_filter_rating", None):
      score += self.options.msg_filter_rating

    if self.diff_matches and getattr(self.options, "diff_filter_rating", None):
      score += self.options.diff_filter_rating

    return score

  def __str__(self):
    """Returns a string representation of this object
    """

    result = []

    for branch in self.branches:
      result.append("In branch: '%s'." % branch)

    for commit in self.reverts:
      result.append("Reverts commit: '%s'." % commit)

    if self.msg_matches:
      result.append("Message matches: '%s'." % self.msg_matches)

    if self.diff_matches:
      result.append("Diff matches: '%s'." % self.diff_matches)

    if self.isBugfix():
      result.append("Bugfix rating: %d." % self.bugfixRating())

    res = self.commit + ":"

    for line in result:
      res = "%s\n%s" % (res,line)

    return res

class GitCache():
  """Storage class.

  Fields:
    branch_map: A dictionary of all refs and their hashes.
    parent_list: A dictionary with tuples of refs and their rev-lists.
    raw_diffs: A dictionary with commits and their raw diffs.
    parsed_diffs: A dictionary with commits and their parsed diffs.
    touched_files: A dictionary with files and the commits that touched them.
    pretty_names: A dictionary with commits and their names
  """

  def __init__(self):
    """Initializes the memory to an empty state
    """

    git = Repo(".").git

    self.pretty_names = {}
    self.raw_diffs = {}
    self.parsed_diffs = {}
    self.touched_files = {}

    result = git.for_each_ref("refs/heads",
                              "refs/remotes",
                              format="%(refname)")

    branches = result.split('\n')
    self.branch_map = branch.getBranchMap(branches)
    self.parent_list = branch.getParentList(self.branch_map.values())

    for name, hash in self.branch_map.iteritems():
      # Skip the leading 'refs/[heads|remotes]' prefix
      pos = name.find('/', 5)
      pos += 1
      name = name[pos:]
      self.pretty_names[hash] = name

def aggregateType(options):
  """Uses the specified options to get type information for many commits
  """

  git = Repo(".").git
  result = git.rev_list(options.start_from)
  commits = result.split('\n')

  options = config.extractOptions(options, options_list)

  # Unless overridden by the user ignore parents for large repositories
  if len(commits) > 1000 and options.ignore_parents == None:
    options.ignore_parents = True

  memory = GitCache()

  result = []

  if options.limit:
    commits = commits[:options.limit]

  for commit in commits:
    if options.debug:
      print(commit)

    stats = determineType(commit, memory, options)
    result.append(stats)

  return result

def determineType(target, memory, options):
  """Determines the type of the specified commit

  Args:
    commit: The commit to determine the type of.
    memory: A class containing various storage containers.
    options: A dictionary containing options.
  """

  branches = branch.belongsTo(target,
                              memory.branch_map,
                              memory.parent_list,
                              pretty_names=memory.pretty_names,
                              ignore_parents=options.ignore_parents)

  reverts = diff.findReverts( target,
                              raw_diffs=memory.raw_diffs,
                              parsed_diffs=memory.parsed_diffs,
                              touched_files=memory.touched_files)

  msg_matches = (options.msg_filter and
      commit.commitmsgMatches(target, options.msg_filter))

  diff_matches = (options.diff_filter and
      commit.commitdiffMatches( target,
                                options.diff_filter,
                                raw_diffs=memory.raw_diffs))

  result = CommitInfo(target, options)

  if not options.branch_filter:
    match = None
  else:
    filter_branches = options.branch_filter.split(':')
    match = set(filter_branches).intersection(set(branches))

  if match:
    result.branches = match

  if reverts:
    result.reverts = reverts

  if msg_matches:
    result.msg_matches = options.msg_filter

  if diff_matches:
    result.diff_matches = options.diff_filter

  return result

def _checkOptions(parser, options):
  """Checks the specified options and uses the parser to indicate errors

  Args:
    parser: The parser to use to signal when the options are bad.
    options: The options to check.
  """

  opts = [options.aggregate, options.type]

  if not parse.isUnique(opts, at_least_one=True):
    parser.error("Please choose exactly one mode")

def dispatch(*args):
  """Dispatches index related commands
  """

  progname = os.path.basename(sys.argv[0]) + " bug"

  parser = OptionParser(option_class=parse.GitOption, prog=progname)

  parser.add_option(
      "-d", "--debug",
      action="store_true",
      help="show debug information")

  parser.add_option(
      "-a", "--aggregate",
      action="store_true",
      help="aggregate bug information of all commits")

  parser.add_option(
      "-t", "--type",
      type="commit",
      help="shows which type the specified commit is")

  parser.add_option(
      "-s", "--start-from",
      type="commit",
      metavar="COMMIT",
      help="the commit to start from")

  parser.add_option(
      "-m", "--message-filter",
      help="mark the commit as a fix if it's log matches this filter")

  parser.add_option(
      "-f", "--diff-filter",
      help="mark the commit as a fix if it's diff matches this filter")

  parser.add_option(
      "-b", "--branch-filter",
      help="mark the commit as a fix if it belongs to this branch")

  parser.add_option(
      "-l", "--limit",
      type="int",
      metavar="N",
      help="limit the commits checked to the most recent N ones")

  parser.add_option(
      "-i", "--ignore-parents",
      type="bool",
      help="whether to enable parentage ignoring")

  # Default to True for now, until there is another option
  parser.set_default("debug", False)
  parser.set_default("aggregate", False)
  parser.set_default("start_from", "HEAD")
  parser.set_default("limit", 10)
  parser.set_default("ignore_parents", None)

  (options, args) = parser.parse_args(list(args))  

  _checkOptions(parser, options)

  if options.aggregate:
    result = aggregateType(options)
  elif options.type:
    kwargs = config.extractOptions(options, options_list)
    memory = GitCache()
    stats = determineType(options.type, memory, kwargs)
    result = [str(stats)]

  for line in result:
    print(line)

