#!/usr/bin/env python

import unittest
import sys

class OutputCatcher:
  """Redirects sys.stdout to self and stores all that is written to it
  """

  def __init__(self):
    """Initilalizes with an empty buffer
    """

    self.buf = []
    self.original = sys.stdout

  def write(self, line):
    """Mandatory write method that stores the written line
    """

    self.buf.append(line)

  def status(self, line):
    """Prints a status line to stdout

    This line is written to the true stdout even if stdout is redirected.

    Args:
      line: The line to be written to stdout.
    """

    self.original.write(line)

  def error(self, line):
    """Prints an error line to stdout

    This line is written to the true stdout even if stdout is redirected.
    The line is prefix with \033[31m to make it show up in red on a terminal
    and suffixed with \033[m to restore to regular color.
    """

    self.original.write("\033[31m" + line + "\033[m")

  def good(self, line):
    """Prints an success line to stdout

    This line is written to the true stdout even if stdout is redirected.
    The line is prefix with \033[32m to make it show up in green on a terminal
    and suffixed with \033[m to restore to regular color.
    """

    self.original.write("\033[32m" + line + "\033[m")

  def release(self):
    """Restores sys.stdout to the original stdout

    When created, or when catch is called, the current
    sys.stdout is stored. Upon calling this method the
    stored stdout is restored.
    """

    sys.stdout = self.original

  def catch(self):
    """Stores the current stdout and redirects it to self
    """

    self.original = sys.stdout
    sys.stdout = self

  def __str__(self):
    """Returns all the output catched so far as one big string
    """

    return "".join(self.buf)

class GitResult(unittest.TestResult):
  """A custom implementation to match Git's test suite's output style
  """

  def __init__(self, output, verbose=False):
    """Initializes all counters to zero
    """

    self.output = output
    self.shouldStop = False
    self.verbose=False
    self.test_count = 0
    self.test_success = 0
    self.test_failure = 0
    self.test_error = 0

    pass

  def writeError(self, test):
    """Outputs an error message for the specified test
    """

    msg = ("* FAIL %d: %s\n\t  %s\n" %
        (self.test_count, test.shortDescription(), test.__str__()))
    self.output.error(msg)

  def addError(self, test, err):
    self.test_error += 1
    self.writeError(test)
    self.output.error("\t  %s\n" % str(err[1]))

  def addFailure(self, test, err):
    self.test_failure += 1
    self.writeError(test)
    self.output.error("\t  %s\n" % str(err[1]))

  def addSuccess(self, test):
    self.test_success += 1
    msg = "*   ok %d: %s\n" % (self.test_count, test.shortDescription())
    self.output.status(msg)

  def startTest(self, test):
    # Check if the description is set
    description = test.shortDescription()
    if not description:
      raise TypeError("Description should be set")

    self.test_count += 1

    print("* expecting success:")
    print("\t" + test.id())

  def wasSuccessful(self):
    return not self.test_failure and not self.test_error

  def testDone(self):
    """Prints a status line
    """

    if self.wasSuccessful():
      msg = "* passed all %d test(s)\n" % self.test_count
      self.output.good(msg)
    else:
      msg = "* "
      if self.test_failure:
        msg += "failed %d test(s)" % self.test_failure
        if self.test_error:
          msg += " and "

      if self.test_error:
        msg += "errored on %d test(s)" % self.test_error

      msg += "\n"

      self.output.error(msg)

class GitTestRunner:
  """A test running class that makes use of GitResult to run the tests
  """

  def run(self, test, verbose=False):
    """Runs the specified test
    """

    # Redired stdout
    catcher = OutputCatcher()
    catcher.catch()

    # But not if verbose
    if self.verbose:
      catcher.release()

    # Set up a result object
    result = GitResult(catcher, verbose)

    # Run the test
    test(result)

    # Restore stdout
    catcher.release()

    # Print the test results
    result.testDone()

    return result

  def runTests(self, options, classes):
    """Runs the specified test in a test suite after parsing args

    Args:
      args: A list of arguments to parse, supported: -v/--verbose
      tests: The tests to run
    """

    self.verbose = options.verbose

    # Create suites from the supplied classes
    loader = unittest.defaultTestLoader

    suites = []

    for testClass in classes:
      suite = loader.loadTestsFromTestCase(testClass)
      suites.append(suite)

    # Create a suite for the tests
    suite = unittest.TestSuite(suites)

    # Run the suite
    res = self.run(suite, verbose=self.verbose)

    if res.wasSuccessful():
      return 0
    else:
      return 1

