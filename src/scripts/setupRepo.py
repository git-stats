#!/usr/bin/env python

import os
import tempfile

from git import Repo

testPath = os.path.join(tempfile.gettempdir(), "testrepo")
metricsPath = os.path.join(tempfile.gettempdir(), "metricsrepo")

class InitializationException(Exception):
  """This exception is raised when something goes wrong during initialization.
  """

def setupRepo(path):
  """Creates a fresh repo under the specified path

  If the specified path exists an exception is raised.

  Args:
    path: The path where the new repo should be created at.
  """

  print("Creating a new repository in " + path)

  # Check if the path exists already, if so bail out
  if os.path.exists(path):
    raise InitializationException(
        "The specified path, " + path + ", exists already, "
        "please remove it or change the target path.")

  # Create a new repo
  repo = Repo.create(path)
  os.chdir(path)

def setDefaultTime():
  """Configures GIT_COMMITTER_DATE and GIT_AUTHOR_DATE to a default value

  The actual date set is 2005-05-26 23:30, which was taken
  from t/t1400-update-ref.sh and then converted to UNIX
  time manually.
  """

  os.environ["GIT_COMMITTER_DATE"] = "1211844600"
  os.environ["GIT_AUTHOR_DATE"] = "1211844600"
  os.environ["TZ"] = "UTC"

def setDefaultInfo():
  """Configures GIT_COMMITTER_NAME etc. to a default value.

  These values are taken from t/test-lib.sh from git.git.
  """

  os.environ["GIT_COMMITTER_NAME"] = "C O Mitter"
  os.environ["GIT_COMMITTER_EMAIL"] = "committer@example.com"
  os.environ["GIT_AUTHOR_NAME"] = "A U Thor"
  os.environ["GIT_AUTHOR_EMAIL"] = "author@example.com"
 
def setSecondaryInfo():
  """Configures GIT_COMMITTER_NAME etc. to a secondary value.

  The values are the default values but in reverse.
  """

  os.environ["GIT_COMMITTER_NAME"] = "Retti Mmoc"
  os.environ["GIT_COMMITTER_EMAIL"] = "retti.mmoc@example.com"
  os.environ["GIT_AUTHOR_NAME"] = "Roh Tua"
  os.environ["GIT_AUTHOR_EMAIL"] = "roh.tua@example.com"

def bumpTime(amount = 60*60):
  """Bumps the date in GIT_AUTHOR_DATE and GIT_COMMITTER_DATE

  Args:
    amount: The amount by which they should be bumped.
  """

  time = os.environ["GIT_AUTHOR_DATE"]
  time = int(time)
  time += amount
  time = str(time)

  os.environ["GIT_AUTHOR_DATE"] = time
  os.environ["GIT_COMMITTER_DATE"] = time


def addFile(filename, createFile=True, commit=True):
  """Adds the specified file

  Args:
    filename: The name of the file to add.
    createFile: Whether the file should be created.
  """

  if createFile:
      file = open(filename, "w")
      file.close()

  git = Repo(".").git

  git.add(filename)

  if commit:
    git.commit("-m", "Added file " + filename)
    bumpTime()

def deleteFile(filename, leaveOnDisk=False):
  """Deletes the specified file

  Args:
    filename: The name of the file to delete.
    leaveOnDisk: Whether the file should not be physically deleted.
  """

  args = []
  
  # If the file should not be deleted, tell 'git rm' so
  if leaveOnDisk:
    args.append("--cached")

  args.append(filename)

  git = Repo(".").git

  git.rm(*args)

  git.commit("-m", "Deleted file " + filename)
  bumpTime()

def createLinearHistory(filename, createFile=False, initialCommit=False, start=1, count=10, finalCommit=False):
  """Creates a linear history in the directory of the current repository.

  The history is fairly simple and is all acted upon the specified file.
  Any exceptions thrown during IO operations are _not_ cought.

  Params:
    filename: The name of the file in which to generate the history.
    createFile: Whether the specified file has to be created first.
    initialCommit: Whether to create an initial commit.
    start: Which commit to start the history at.
    count: The size of the history.
    finalCommit: Whether to create a final commit.
  """

  git = Repo(".").git

  stop = start + count

  if createFile:
    addFile(filename, commit=False)

  # Create or open the content file
  file = open(filename, "a")

  if initialCommit:
    # Start out with an initial change
    file.write("Initial change\n")
    file.flush()

    # Add the file and create the initial commit
    git.commit("-a", "-m", "Initial commit")
    bumpTime()

  # Create a linear history
  for i in range(start, stop):
    file.write("Change " + str(i) + "\n")
    file.flush()

    git.commit("-a", "-m", "Commit " + str(i))
    bumpTime()

  if finalCommit:
    # Finish it up with a final change
    file.write("Last change\n")
    file.flush()

    # And a final commit
    git.commit("-a", "-m", "Last commit")
    bumpTime()

def createBranch(name, checkout=True):
  """Creates a branch with the specified name and optionally checks it out

  Params:
    name: The name of the new branch.
    checkout: Whether to perform a checkout of the branch.
  """

  git = Repo(".").git

  git.branch(name)
  bumpTime()

  if checkout:
    checkoutBranch(name)

def deleteBranch(name):
  """Deletes a branch

  The branch that is to be deleted should be a subset of the current branch.

  Params:
    name: The name of the branch to delete.
  """

  git = Repo(".").git

  git.branch("-d", name)
  bumpTime()

def createDir(name, changeDir=False):
  """Creates a directory with the specified name

  Args:
    name: The name of the directory
  """

  os.mkdir(name)

def checkoutBranch(name):
  """Switches to the specified branch
  
  Params:
    name: The name of the branch to be switched to.
  """

  git = Repo(".").git

  git.checkout(name)

def checkoutRelease(release):
  """Switches to the specified release

  Params:
    release: The release to check out
  """

  checkoutBranch('v' + str(release))

def mergeBranch(name):
  """Merges the current branch with the specified branch

  Params:
    name: The name of the branch to merge with.
  """

  git = Repo(".").git

  git.merge(name)
  bumpTime()

def revertLastCommit(commitChanges=True):
  """Reverts the last commit made

  Params:
    commitChanges: Whether to commit the revert.
  """

  git = Repo(".").git

  options = ["--no-edit", "HEAD"]

  if not commitChanges:
    options.append("-n")

  git.revert(*options)

  if commitChanges:
    bumpTime()

def tagRelease(releaseNumber, head="HEAD"):
  """Tags a release.
  
  The created tag is 'v' + releaseNumber.

  Params:
    releaseNumber: The number of the release.
  """

  git = Repo(".").git

  git.tag('v' + str(releaseNumber), head)

def createRemoteBranch(name, start="HEAD"):
  """Creates a new remote branch with the specified name

  Args:
    name: The plain name of the remote (no '/refs/remotes/' prefix)
    start: The revision to branch off from
  """

  git = Repo(".").git

  git.update_ref('refs/remotes/' + name, start)
  bumpTime()

def checkHead(HEAD):
  """Verifies that the HEAD is as expected.

  Params:
    HEAD: The expected value of HEAD.
  """

  git = Repo(".").git

  result = git.rev_parse("HEAD")
  
  # Eat the trailing newline
  currentHEAD = result[:-1]

  scriptSuccessful = (HEAD == currentHEAD)

  if scriptSuccessful:
    print("Done, repo created successfully")
    return True
  else:
    print("Something went wrong, current HEAD doesn't match.")
    print("Expected: '" + HEAD + "'.")
    print("Actual: '" + currentHEAD + "'.")
    return False

def createTestRepository():
  """Creates a test repository under setupRepo.testpath
  """

  # Directories
  d = "docs"
 
  # Files
  c = "content.txt"
  n = "notes.txt"
  t = "test.c"
  r = os.path.join(d, "README.txt")

 # Branches
  m = "master"
  mt = "maint"
  p = "pu"
  e = "erase"
  j = "junio"

  # Start afresh
  setupRepo(testPath)

  # Set the default author/committer
  setDefaultInfo()
  setDefaultTime()

  # Create some linear history
  createLinearHistory(c, createFile=True, initialCommit=True, finalCommit=True)

  # Create a maintenance branch
  createBranch(mt)

  # Create some history there too
  createLinearHistory(n, createFile=True, count=3)

  # Go back to master and merge with maint
  checkoutBranch(m)
  mergeBranch(mt)

  # Create a playground branch and create some history
  # This branch will be left 'dead', e.g., unused after this
  createBranch(p)
  createLinearHistory(t, createFile=True, count=7, finalCommit=True)

  # Start out with release 0
  tagRelease(0, head="HEAD~3")

  # Revert that commit
  revertLastCommit()

  # Go back to master and continue some history
  checkoutBranch(m)
  createLinearHistory(c, start=10, count=5)

  # Yay, our first release!
  tagRelease(1)

  # Merge current master into maint
  checkoutBranch(mt)
  mergeBranch(m)

  # Ouch! Brown paper bag fix there, correct it and merge into master
  revertLastCommit(commitChanges=False)
  createLinearHistory(c, start=42, count=1)
  checkoutBranch(m)
  mergeBranch(mt)

  # Continue some work on master
  createLinearHistory(c, start=16, count=6)

  # Have someone else do some work on master
  setSecondaryInfo()
  createLinearHistory(c, start=22, count=2)

  # Go back to the initial data
  setDefaultInfo()

  # Create a directory for the documentation
  createDir(d)

  # Create a readme and add some content to it
  createLinearHistory(r, createFile=True, count=5, finalCommit=True)
  tagRelease(2)

  # Ah, but that last one was bad, we don't want it
  revertLastCommit()

  # Instead, continue the linear history
  createLinearHistory(r, start=6, count=1)
  tagRelease(3)

  # Come to think of it, that -was- a really good change after all
  revertLastCommit()

  # Reinstate that final commit
  createLinearHistory(r, count=0, finalCommit=True)

  tagRelease(4, head="HEAD~1")

  # Create a branch to delete a file on
  createBranch(e)

  # Remove a file from git
  deleteFile(r, leaveOnDisk=True)  

  # Only to add it back later so that we don't have any dangling files
  addFile(r, createFile=False)

  # Create a release at this point for easy switching later
  tagRelease(5)

  # Switch back to master for the test suite
  checkoutBranch(m)

  # Create a remote branch
  createRemoteBranch(j, start="HEAD^")

  return 0

def createMetricsRepository():
  """Creates a metrics repository in setupRepo.metricspath
  """

  # files
  c = "content.txt"
  n = "notes.txt"
  r = "README"
  l = "limbo.dat"

  # branches
  m = "master"
  mt = "maint"
  h = "help"
  s = "side"

  # Create a repository to work in
  setupRepo(metricsPath)

  # Set the info
  setDefaultInfo()
  setDefaultTime()

  # And create some history
  createLinearHistory(c, createFile=True, count=2)

  # Mark this commit so that we can jump back to it later
  tagRelease(1)

  # And create some more history
  createLinearHistory(c, start=3, count=3)

  # Go back to that marked commit
  checkoutRelease(1)

  # Now create a new branch to work on
  createBranch(mt)

  # Fix us up some more history
  createLinearHistory(n, createFile=True, count=2)

  # Mark this commit too so we can jump back
  tagRelease(2)

  # And make some more history
  createLinearHistory(n, start=2, count=1)

  # Now merge this back into master
  checkoutBranch(m)
  mergeBranch(mt)

  # Go back to maint to create some more there
  checkoutBranch(mt)
  createLinearHistory(n, start=3, count=1)

  # Now let's go back and fix that one commit
  checkoutRelease(2)
  createBranch(h)
  createLinearHistory(r, createFile=True, count=2)

  # Go back to maint, and merge it in
  checkoutBranch(mt)
  mergeBranch(h)

  # Now that we merged that one in, kick it out
  deleteBranch(h)

  # We can create some more history here
  createLinearHistory(n, start=4, count=3)

  # Remember for branching off later
  tagRelease(3)

  # And a healthy bit of content on master too
  checkoutBranch(m)
  createLinearHistory(c, start=6, count=4)

  # Create one more commit
  checkoutBranch(mt)
  createLinearHistory(n, start=7, count=1)

  # Merge master in here
  mergeBranch(m)

  # And create another commit
  createLinearHistory(n, start=8, count=1)

  # Hacking on master some more
  checkoutBranch(m)
  createLinearHistory(c, start=10, count=4)

  # Go back a bit and branch off
  checkoutRelease(3)
  createBranch(s)
  createLinearHistory(l, createFile=True, count=3)

  # Merge back into maint and work some more
  checkoutBranch(mt)
  mergeBranch(s)
  createLinearHistory(n, start=9, count=1)

  # Merge that into master and finish up
  checkoutBranch(m)
  mergeBranch(mt)
  createLinearHistory(c, start=14, count=1)
  
  # Finish side branch
  checkoutBranch(s)
  createLinearHistory(l, start=4, count=4)

  # Lastly finish up maint
  checkoutBranch(mt)
  createLinearHistory(n, start=10, count=2)

  return 0

def main(args):
  """Creates a test and a metrics repository

  Args:
    args: An array of arguments, when 2 in size the second
    element should either be 'test' to just create the test
    repo, or 'metrics' to just create the metrics
    repository. If both should be created then the size of
    args should be 1. When the third argument is 'path',
    instead of creating the repository the path where it
    will be created is printed. If there is a fourth
    argument it is interpreted as the path where the
    repository should be created. The first element in the
    array is always ignored.

  Returns: 0 upon success, or nonzero upon failure.
  """

  global testPath
  global metricsPath

  size = len(args)

  def printUsage():
    str = "Please specify either 'test', 'metrics', or nothing to run both." \
    "\nSuffix with 'path' to print the path where the repo will be created." \
    "\nAs third argument you may provide the path to create the repo in."

    print(str)

  if size > 4:
    printUsage()
    return 129

  if size == 3:
    if args[2] != "path":
      printUsage()
      return 129

  createTestRepo = False
  createMetricsRepo = False
  printTestRepoPath = False
  printMetricsRepoPath = False

  if size == 1:
    createTestRepo = True
    createMetricsRepo = True

  if size == 2 or size == 4:
    if args[1] == "test":
      createTestRepo = True

    if args[1] == "metrics":
      createMetricsRepo = True

  if size == 3:
    if args[1] == "test":
      printTestRepoPath = True

    if args[1] == "metrics":
      printMetricsRepoPath = True

  if not createTestRepo and not createMetricsRepo \
      and not printTestRepoPath and not printMetricsRepoPath:
        printUsage()
        return 129

  if createTestRepo:
    if size == 4:
      testPath = args[3]

    ret = createTestRepository()
    if ret != 0:
      return ret

  if createMetricsRepo:
    if size == 4:
      metricsPath = args[3]

    ret = createMetricsRepository()
    if ret != 0:
      return ret

  if printTestRepoPath:
    print(testPath)

  if printMetricsRepoPath:
    print(metricsPath)

  return 0

if __name__ == '__main__':
  import sys
  ret = main(sys.argv)
  sys.exit(ret)

