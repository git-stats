#!/usr/bin/env python

"""This module contains commands related to statistics on git repositories.

Each command here is merely a wrapper around other, more complicated, commands.
"""

import os

# Figure out what we are running from
# Then get the directory we are in and use that instead
# Ofcourse, this only works if git_python is in the dir we are run from

file = __file__
path = os.path.abspath(file)
dir = os.path.dirname(path)

os.sys.path.insert(0, dir)

from git import Git

from git_stats import author
from git_stats import bug
from git_stats import branch
from git_stats import commit
from git_stats import diff
from git_stats import index
from git_stats import matcher
from git_stats import tests

def version(*args):
  """Shows a silly version message
  """

  print("Current version is 0.1-alpha_pre_dinner_build")
  return 0

class DispatchException(Exception):
  """This exception is raised when something went wrong during dispatching
  """

  pass

class Dispatcher():
  """This class provides basic dispatching functionality
  """


  def __init__(self, commands):
    self.commands = commands

  def showUsageMessage(self):
    """Shows a usage message, listing all available commands
    """

    print("Available commands are:")

    for key in sorted(self.commands):
      print(key)

  def dispatch(self, argv):
    """Dispatches a command with the specified arguments

    Args:
      argv: The arguments to parse and dispatch.
    """

    if not len(argv) > 0:
      self.showUsageMessage()
      return 1

    command = argv[0]
    
    for key, value in self.commands.iteritems():
      if key.startswith(command):
        func = value
        break
    else:
      raise DispatchException("Unknown command '" + command + "'.")

    # When not specifying a command, throw in the --help switch
    if len(argv) == 1:
      argv.append("--help")

    return func(*argv[1:])

commands = {
  "author" : author.dispatch,
  "bug"    :    bug.dispatch,
  "branch" : branch.dispatch,
  "commit" : commit.dispatch,
  "diff"   :   diff.dispatch,
  "index"  :  index.dispatch,
  "matcher":matcher.dispatch,
  "tests"  :  tests.dispatch,
  "-v"     :         version,
}

def main(args):
  """Main routine, dispatches the command specified in args

  If called with sys.argv, cut off the first element!
  """

  result = Dispatcher(commands).dispatch(args)
  return result

if __name__ == '__main__':
  import sys
  result = main(sys.argv[1:])
  sys.exit(result)

