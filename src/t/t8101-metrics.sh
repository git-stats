#!/bin/sh
#
# Copyright (c) 2008 Sverre Rabbelier
#

test_description='stats tests (basics)

This test verifies that the basic stats commands work as expected.'

ORIG_PATH=$PWD
PATH=$ORIG_PATH/..:$PATH

. ./test-lib.sh

export PATH

###########
# Setup
###########

test_expect_success 'extract test repo path' '
    METRICS_REPO_PATH=$(python ../scripts/setupRepo.py metrics path) || {
      trap - exit
      echo >&5 "FATAL: Cannot extract test repo path"
      exit 1
    }
'

test_expect_success 'pre-clean' '
    rm -fr "$METRICS_REPO_PATH" || {
	    trap - exit
	    echo >&5 "FATAL: Cannot clean the fresh repo"
	    exit 1
    }
'

test_expect_success 'setup' '
    python ../scripts/setupRepo.py metrics path "$METRICS_REPO_PATH" && \
    cd "$METRICS_REPO_PATH" || {
      trap - exit
      echo >&5 "FATAL: Cannot cd into fresh repo"
      exit 1
    }
'

test_expect_success 'Test that stats.py is reachable' '
    stats.py -v || {
      trap - exit
      echo >&5 "FATAL: stats.py not reachable"
      exit 1
    }
'

echo "e5aa8fcf1233ea51764bad0d929ed15dc5c90dc1" > expected

test_expect_success 'Test that the expected tree was created by setupRepo' '
    git rev-parse HEAD > actual
    test_cmp expected actual || {
      trap - exit
      echo >&5 "FATAL: Head revision does not match"
      exit 1
    }
'


###########
# Belongs to
###########

echo "Matching branches:
master
maint
side" > expected

test_expect_success 'Test that the initial revision is owned by all' '
    stats.py branch -b a23d994 > actual
    test_cmp expected actual    
'

echo "Matching branches:
master" > expected

test_expect_success \
'Test that a commit made on the master branch belongs only to master' \
'
    stats.py branch -b 2e560d2 > actual
    test_cmp expected actual
'

test_expect_success \
'Test that the dilution is correct when another branch is merged in later on' \
'
    stats.py branch -b 866d9d7 > actual
    test_cmp expected actual
'

echo "Matching branches:
maint
side" > expected

# It should belong to both maint and side since side was
# branched off from maint after the tested commit was made.
# Tested are: 
# * The point where side branched off from maint
# * A point before the branch off point
# * A commit on a side branch that was merged in
test_expect_success \
'Test that a commit made on maint belongs to both maint and side' \
'
    stats.py branch -b 38c3784 > actual1
    stats.py branch -b 9a0b6cc > actual2
    stats.py branch -b 442be4c > actual3
    test_cmp expected actual1 &&
    test_cmp expected actual2 &&
    test_cmp expected actual3
'

echo "Matching branches:
maint" > expected

test_expect_success \
'Test that a merge commit belongs to the branch the merge was done on' \
'
    stats.py branch -b 4ff12b2 > actual
    test_cmp expected actual
'

echo "Matching branches:
side" > expected

test_expect_success \
'Test that the base of a merge belongs to the branch the merge was based on' \
'
    stats.py branch -b 40843d5 > actual
    test_cmp expected actual
'

test_expect_success \
'Test that the dilution is correct when other branches merge with its branch' \
'
    stats.py branch -b bb33bd1 > actual
    test_cmp expected actual
'

test_expect_success \
'Test that a branch made on side, and not merged later, belongs to side' \
'
    stats.py branch -b 698a02f > actual
    test_cmp expected actual
'


###########
# Teardown
###########


test_expect_success 'Teardown' '
  cd $ORIG_PATH
'

test_done

