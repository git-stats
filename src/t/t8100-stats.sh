#!/bin/sh
#
# Copyright (c) 2008 Sverre Rabbelier
#

test_description='stats tests (basics)

This test verifies that the basic stats commands work as expected.'

ORIG_PATH=$PWD
PATH=$ORIG_PATH/..:$PATH

. ./test-lib.sh

export PATH GIT_STATS_PRETTY_PRINT="oneline"

###########
# Setup
###########

test_expect_success 'extract test repo path' '
    TEST_REPO_PATH=$(python ../scripts/setupRepo.py test path) || {
      trap - exit
      echo >&5 "FATAL: Cannot extract test repo path"
      exit 1
    }
'

test_expect_success 'pre-clean' '
    rm -fr "$TEST_REPO_PATH" || {
	    trap - exit
	    echo >&5 "FATAL: Cannot clean the fresh repo"
	    exit 1
    }
'

test_expect_success 'setup' '
    python ../scripts/setupRepo.py test path "$TEST_REPO_PATH" && \
    cd "$TEST_REPO_PATH" || {
      trap - exit
      echo >&5 "FATAL: Cannot cd into fresh repo"
      exit 1
    }
'

test_expect_success 'Test that stats.py is reachable' '
    stats.py -v || {
      trap - exit
      echo >&5 "FATAL: stats.py not reachable"
      exit 1
    }
'

echo "f1fdf284733011b1172b4517b4526879059bb414" > expected

test_expect_success 'Test that the expected tree was created by setupRepo' '
    git rev-parse HEAD > actual
    test_cmp expected actual || {
      trap - exit
      echo >&5 "FATAL: Head revision does not match"
      exit 1
    }
'


###########
# Author tests
###########

echo "  23:    23+     1-    22~ = content.txt
  10:     8+     2-     6~ = docs/README.txt
   3:     3+     0-     3~ = notes.txt" > expected

test_expect_success \
'Test that "author -d" filters as expected for the primary author' \
'
    stats.py author -d "author@example.com" > actual
    test_cmp expected actual
'

echo "   2:     2+     0-     2~ = content.txt" > expected

test_expect_success \
'Test that "author -d" filters as expected for the secondary author' \
'
    stats.py author -d "roh.tua@example.com" > actual
    test_cmp expected actual
'

echo "author@example.com:
	  23:    23+     1-    22~ = content.txt
	  10:     8+     2-     6~ = docs/README.txt
	   3:     3+     0-     3~ = notes.txt

roh.tua@example.com:
	   2:     2+     0-     2~ = content.txt
" > expected

test_expect_success \
'Test that "author -e" shows stats for all authors' \
'
    stats.py author -e > actual
    test_cmp expected actual
'

echo "author@example.com: 24
roh.tua@example.com: 2" > expected

test_expect_success 'Test that "author -f" aggregates as expected' '
    stats.py author --file=content.txt > actual
    test_cmp expected actual
'

echo "  25:    25+     1-    24~ = content.txt
  10:     8+     2-     6~ = docs/README.txt
   3:     3+     0-     3~ = notes.txt" > expected

test_expect_success 'Test that "author -a" aggregates as expected' '
    stats.py author -a > actual
    test_cmp expected actual 
'

###########
# Branch tests
###########

{
  echo "Matching branches:"
  echo "pu"
} > expected

test_expect_success 'Test that "branch -c" filters as expected' '
    stats.py branch -c v0 > actual
    test_cmp expected actual
'

{
  echo "Matching branches:"
  echo "erase"
  echo "maint"
  echo "master"
} > expected

test_expect_success \
'Test that "branch -c" filters as expected when multiple branches match' \
'
    stats.py branch -c v1 > actual
    test_cmp expected actual
'

echo "junio" >> expected

test_expect_success 'Test that "branch -r" works as expected' '
    stats.py branch -r -c v1 > actual
    test_cmp expected actual
'


###########
# Commit tests
###########


echo "3b0e7ae64f1b39f7b23d789a9d472443dd00c6fb Commit 3
9ae22dc6a76c6ac715cc9fe6a011156ca042f480 Commit 2
ae9fd667fe1611e0648a2ebec65788b63bb3ce16 Commit 1" > expected

test_expect_success 'Test that "commit -t" filters as expected' '
    stats.py commit -t notes.txt > actual
    test_cmp expected actual
'

test_expect_success 'commit tests setup' '
    cd docs
'

echo 'f1fdf284733011b1172b4517b4526879059bb414 Last commit
4de759680bf8bd7b51e3a0481d027ee441998542 Revert "Commit 6"
255ef82de488015b727e0202f0dfe8ed82443ebb Commit 6
4838591dcda04acd0e8882397c5c681af09c9076 Revert "Last commit"
4b650481338f95cd8afbc113bd15cd85d94c0c3a Last commit
37837a72217f8da3f056d5aa8b159412b93efd69 Commit 5
a03fc7e69239cc3e29c96ebf74e7d22e2d72bbef Commit 4
39f4923417895119ac26524c572dbd198570f054 Commit 3
3302051a5e010ede6cdb6d8fa66cff7e89f63b28 Commit 2
7bd493f55b8b0b0c59c08ca5be7c8f98fb159885 Commit 1' > expected

test_expect_success 'Test that "commit -r" works as expected' '
    stats.py commit -r -t README.txt > actual
    test_cmp expected actual
'

test_expect_success \
'Test that "commit -r" works as expected when the file was not found' \
'
    test_must_fail stats.py commit -t README.txt
'

test_expect_success 'commit tests teardown' '
    cd ..
'

echo "9ebfa8e247b99c0ba0198244d574734899dac987 Initial commit" > expected

test_expect_success 'Test that "commit --log" filters as expected' '
    stats.py commit --log-contains=Initial > actual
    test_cmp expected actual
'

test_expect_success 'Test that "commit --diff" filters as expected' '
    stats.py commit --diff-contains=Initial > actual
    test_cmp expected actual
'


###########
# Diff tests
###########


echo "Missing the following keys:
('a/test.c', 'b/test.c')" > expected

test_expect_success \
'Test that "diff -e" works as expected for equal diffs in different files' \
'
    stats.py diff -e v0 v4 > actual
    test_cmp expected actual
'

echo "Missing the following keys:
('a/docs/README.txt', 'b/docs/README.txt')" > expected

test_expect_success \
'Test that "diff -e" works as expected for equal diffs in different files' \
'
    stats.py diff -e v4 v0 > actual
    test_cmp expected actual
'

echo "Equal" > expected

test_expect_success 'Test that "diff -e" works as expected for equal diffs' '
    stats.py diff -e HEAD HEAD > actual
    test_cmp expected actual
'

test_expect_success 'Test that "diff -n -e" works as expected' '
    stats.py diff -n -e v2 v3 > actual
    test_cmp expected actual
'

echo "Equal" > expected

test_expect_success 'Test that "diff -i" works as expected for reverts' '
    stats.py diff -i -e v2 v3^ > actual
    test_cmp expected actual
'


###########
# Index tests
###########


test_expect_success 'index test setup' '
    echo "Some Change" >> notes.txt
    git add notes.txt
'

echo "3b0e7ae64f1b39f7b23d789a9d472443dd00c6fb Commit 3
9ae22dc6a76c6ac715cc9fe6a011156ca042f480 Commit 2
ae9fd667fe1611e0648a2ebec65788b63bb3ce16 Commit 1" > expected

test_expect_success \
'Test that "index -t" works as expected when there are staged changes' \
'
    stats.py index -t > actual
    test_cmp expected actual
'

test_expect_success 'index test teardown' '
    git reset --hard HEAD
'

test_expect_success 'index -a test setup' '
    git checkout erase
    git reset --soft v5^
'

{
echo "No changes staged, no matching commits, or only new files added"
} > expected

test_expect_success 'Test that "index -t" without -a works as expected' '
    stats.py index -t > actual
    test_cmp expected actual
'

echo '28b2600cf58d478b4b937262218e363c88756b89 Deleted file docs/README.txt
f1fdf284733011b1172b4517b4526879059bb414 Last commit
4de759680bf8bd7b51e3a0481d027ee441998542 Revert "Commit 6"
255ef82de488015b727e0202f0dfe8ed82443ebb Commit 6
4838591dcda04acd0e8882397c5c681af09c9076 Revert "Last commit"
4b650481338f95cd8afbc113bd15cd85d94c0c3a Last commit
37837a72217f8da3f056d5aa8b159412b93efd69 Commit 5
a03fc7e69239cc3e29c96ebf74e7d22e2d72bbef Commit 4
39f4923417895119ac26524c572dbd198570f054 Commit 3
3302051a5e010ede6cdb6d8fa66cff7e89f63b28 Commit 2
7bd493f55b8b0b0c59c08ca5be7c8f98fb159885 Commit 1' > expected

test_expect_success 'Test that "index -a" works as expected' '
    stats.py index -a -t > actual
    test_cmp expected actual
'

test_expect_success 'index -a test teardown' '
    git reset --hard v5
    git checkout master
'


###########
# Teardown
###########


test_expect_success 'Teardown' '
  cd $ORIG_PATH
'

test_done

