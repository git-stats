The following metrics will be used to determine if something is a fix:
* If a commit is made on a specific branch after a specific commit.
  
  For example, all commits that are made in a maintenance
  branch are guaranteed to be fixes. The problem here is
  that usually  a maintenance branch is 'branched off' the
  mainstream branch at the point of a release, as such 
  there has to be a lower limit (say, the 'creation point'
  of the branch) from where on commits are seen as 'part of
  the maintenance branch'. This means that we need a
  mechanism to determine whether a commit belongs on a
  branch or not. This can be done by determining how
  'diluted' the commit is at the head of a branch. Here we
  define 'diluting' as merging, if a commit is part of a
  branch only because it got merged in, that is a strong
  dilution. If another branch is merged in that is only a
  slight dilution (or perhaps none at all).

  We want to create a test repository with a structure
  similar to what is depicted below.

  o-o-o-1-o-o-o-o-o-o-o-o-5-o-o-o-A
     \     /         \       /
      o-2-o-o-4-o-o-o-o-o-o-7-o-o-B
         \   /     \     /
          3-o       o-6-o-o-o-8-o-C

  Main points of interest are the numbered commits.
  This repository will serve as testing ground for the
  'belongs to a branch' metric.
  1. It is evident this commit 1 belongs to branch A, since
  that is the branch it was made on.
  2. Could be seen as belonging to either branch A, B or C,
  since all of those branches contain it. However, it
  should belong 'most' to branch B, since that is the
  branch it was made on, whereas it was 'only' merged into
  branch A and C.
  3. Belongs 'most' to branch B, since the branch it got
  made on was merged into B and then got deleted. Whereas
  branch A and C got it through a merge.
  4. A merge commit, it merges commit 3 into branch B and
  is only of interest because merge commits might need
  extra attention.
  5. Belongs to branch A since it is the only branch it
  exists on, but does occur before a merge with branch
  B, this should not change it's dilution though.
  6. Contained in all branches, but should belong most to
  branch C, then to B, and only lastly to branch A.
  7. Interesting because it is a merged commit, belongs to
  branch B, although branch A also contains it.
  8. A single commit on branch C, should obviously only
  belong to branch C with no dilution.
 
* If the commit message matches a certain regexp.

  For example, if the commit message contains the line:
  'this fixes', it is a fix. The regexp can be as simple
  as matching 'fix ' or 'fixes ', there will probably
  be little need for advanced regexp machinery.

* If the diff contains a specific change, matching a certain regexp.
  
  For example, in a test suite there might be an indicator
  for known breakages, when this indicator is changed from
  'known breakage' to 'should pass' the commit fixed the
  known breakage.

* If the commit reverts another commit.

  Whenever a commit reverts another one, the reverted
  commit was a mistake, and as such the reverting commit is
  a fix. By first checking which commits touch the same
  files the commits that have to be checked can be reduced
  to a feasible amount.

* If a commit touches nearly all the same lines as another commit
  
  Commits that are not actual reverts, but that do touch
  the same files are very likely to be fixes. But, if the
  commit is from the same author, made shortly after the
  first commit, it is likely to be from the same 'patch
  series' and as such should not be treated as a fix.

