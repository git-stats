syntax: stats.py bug <options>

The purpose of the bug module is to gather statistics on
bugfixes within the content, and to aggregate this
information to provide with a report of the last N commits.

The metrics used in this module are:
* Does a commit belong to a specific branch. This can be
used, for example, to mark commits on a maintenance as
bugfixes with 99% reliability. Whenever multiple refs point
to the same commit though, the first ref passed by
'git for-each-ref' will be listed if that commit is the one
the searched commit belongs to. As such, you need to make
sure your regexp catches those refs.

* Does a commit fully revert another one. That is, if you
make a commit, and then revert it, the revert will be
detected as having reverted the earlier commit.

* Does the commit message match a certain regexp. For
example, if it contains the word "fixes", mark it as a fix.

* Does the commit diff match a specified regexp. A change
from "test_expect_failure" -> "test_expect_success" could
indicate that the commit is a bugfix.

- (not tweaked yet) Does a commit partially revert another
one. This needs some tweaking, otherwise small changes are
quickly seen as similar. (If you have two unrelated
one-liners, and you set it to ignore one difference...)

You have to configure a few things to get useful results.
Configuration is trivially done with:
'git config GitStats.key value'. It is also possible to
override most options on the command-line when running the
metric. The *_rating settings can be set to any integer,
the relevance is in their value relative to one another.
The value of each *_rating setting should represent how
confident you are that a commit is a bugfix when that
specific metric returns a positive result.

The following options are available:
* 'GitStats.branch_filter', this defines the regexp that,
when matched, indicates that a branch is a maintenance
branch. When not set, the branch check will be skipped.
Example value: 'maint'

* 'GitStats.branch_filter_rating', the amount by which the
'bugfix rating' should be increased when the above filter
matches on a certain commit.

* 'GitStats.debug', whether to enable debug information.

* 'GitStats.diff_filter', this defines the regexp that,
when matched, indicates that a commit is a bugfix.
Example value: 'test_expect_failure'

* 'GitStats.diff_filter_rating', the amount by which the
'bugfix rating' should be increased when the above filter
matches on a certain commit.

* 'GitStats.msg_filter', this defines the regexp that,
when matched, indicates that a commit is a bugfix.
Example value: 'fixes'

* 'GitStats.msg_filter_rating', the amount by which the
'bugfix rating' should be increased when the above filter
matches on a certain commit.

* 'GitStats.ignore_parents', when set, the ancestry of each
commit will be retrieved during the 'belongs to' metric,
which cuts down execution time by a lot on large repo's,
but causes a slow down on small repo's. When unset, it
defaults to disabled for repo's with less than 1000
commits, and is enabled for those larger than 1000.

* 'GitStats.limit', the amount of commits that should be
examined by default by 'stats.py bug -a', this defaults
to 10, but can be set to a higher value for small repo's.
Example value: For large repositories large value's might
result in slow execution, the default value is pretty
sane for such cases. Smaller repositories might benefit
from setting it to a high value, or disabling it (by
setting it to 0).

* 'GitStats.revert_rating', the amount by which the
'bugfix rating' should be increased when the above filter
matches on a certain commit.

Currently the available metrics in the bug module are the
following:
* Determine whether a specific commit is a bugfix based on
  other metrics. When one of the metrics is 'positive',
  that is, it's return value indicates that the examined
  commit is a bugfix, the 'bugfix rating' is increased by
  a pre-configured amount. This amount can be specified per
  metric, and can be set to '0' to ignore it.

* Aggregate the above metric over the past N commits. Also,
  when running the above metric on more than one commit,
  cache the result of calls to the git binary so that the
  execution time is reduced. This means that the execution
  time is not directly proportional to the size of the
  repository. (Instead, there is a fixed 'start up' cost,
  after which there is a 'per commit' cost, which is
  relatively low.)

This module does not define any auxiliary functions.

